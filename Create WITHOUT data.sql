-- phpMyAdmin SQL Dump
-- version 4.0.4.1
-- http://www.phpmyadmin.net
--
-- Machine: 127.0.0.1
-- Genereertijd: 05 nov 2013 om 20:03
-- Serverversie: 5.5.32
-- PHP-versie: 5.4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Databank: `project_festival`
--
CREATE DATABASE IF NOT EXISTS `project_festival` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `project_festival`;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `t_agegroup`
--

CREATE TABLE IF NOT EXISTS `t_agegroup` (
  `ageGroupId` int(11) NOT NULL AUTO_INCREMENT,
  `endAge` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `startAge` int(11) DEFAULT NULL,
  PRIMARY KEY (`ageGroupId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `t_artist`
--

CREATE TABLE IF NOT EXISTS `t_artist` (
  `artistId` int(11) NOT NULL AUTO_INCREMENT,
  `managerName` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `artistInfoId` int(11) DEFAULT NULL,
  `facilityId` int(11) DEFAULT NULL,
  PRIMARY KEY (`artistId`),
  KEY `FK_576rbi5n6f19t66ek0e1a9hm8` (`artistInfoId`),
  KEY `FK_k81dfixb9m3r1l686d9l5d4hh` (`facilityId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=39 ;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `t_artistinfo`
--

CREATE TABLE IF NOT EXISTS `t_artistinfo` (
  `artistInfoId` int(11) NOT NULL AUTO_INCREMENT,
  `biography` longtext,
  `permission` tinyint(1) DEFAULT NULL,
  `festivalId` int(11) DEFAULT NULL,
  PRIMARY KEY (`artistInfoId`),
  KEY `FK_kqn2mamn94343j8sefjbpoa9a` (`festivalId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=59 ;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `t_employee`
--

CREATE TABLE IF NOT EXISTS `t_employee` (
  `employeeId` int(11) NOT NULL AUTO_INCREMENT,
  `address` varchar(255) DEFAULT NULL,
  `firstName` varchar(255) DEFAULT NULL,
  `lastName` varchar(255) DEFAULT NULL,
  `phoneNumber` varchar(255) DEFAULT NULL,
  `festivalId` int(11) DEFAULT NULL,
  PRIMARY KEY (`employeeId`),
  KEY `FK_axqg68nmgo3w8p03993c0bgv8` (`festivalId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `t_employee_task`
--

CREATE TABLE IF NOT EXISTS `t_employee_task` (
  `employeeId` int(11) NOT NULL,
  `taskId` int(11) NOT NULL,
  KEY `FK_81sd9fumstbw8ae52lk5491xs` (`taskId`),
  KEY `FK_nrenv6wv4e5st3c64ismsngk3` (`employeeId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `t_facility`
--

CREATE TABLE IF NOT EXISTS `t_facility` (
  `facilityId` int(11) NOT NULL AUTO_INCREMENT,
  `color` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `subFacilityId` int(11) DEFAULT NULL,
  `facilityTypeId` int(11) DEFAULT NULL,
  `festivalId` int(11) DEFAULT NULL,
  PRIMARY KEY (`facilityId`),
  KEY `FK_slofsoqfmpisrsu1emhv6bssr` (`subFacilityId`),
  KEY `FK_qrqnr96y8qpc08xw0228aihyk` (`facilityTypeId`),
  KEY `FK_1uxguwea9fbhj8u64eu4k2y5e` (`festivalId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=86 ;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `t_facilitytype`
--

CREATE TABLE IF NOT EXISTS `t_facilitytype` (
  `facilityTypeId` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) DEFAULT NULL,
  `festivalId` int(11) DEFAULT NULL,
  PRIMARY KEY (`facilityTypeId`),
  KEY `FK_smkrcmo5h6h5js7bhmjakq7oi` (`festivalId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `t_facility_visittimes`
--

CREATE TABLE IF NOT EXISTS `t_facility_visittimes` (
  `Facility_facilityId` int(11) NOT NULL,
  `visitTimes` bigint(20) DEFAULT NULL,
  KEY `FK_q3k10u662686ih6qu5ucvhc9i` (`Facility_facilityId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `t_festival`
--

CREATE TABLE IF NOT EXISTS `t_festival` (
  `festivalId` int(11) NOT NULL AUTO_INCREMENT,
  `endTime` datetime DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `startTime` datetime DEFAULT NULL,
  `totalSold` double DEFAULT NULL,
  `locationId` int(11) DEFAULT NULL,
  PRIMARY KEY (`festivalId`),
  KEY `FK_mdqyn08iijgjtvixvj1hgf0yc` (`locationId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `t_festivalday`
--

CREATE TABLE IF NOT EXISTS `t_festivalday` (
  `festivalDayId` int(11) NOT NULL AUTO_INCREMENT,
  `date` date DEFAULT NULL,
  `festivalId` int(11) DEFAULT NULL,
  PRIMARY KEY (`festivalDayId`),
  KEY `FK_1c64ckhitpv6u66a2rsjmplls` (`festivalId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `t_festival_agegroups`
--

CREATE TABLE IF NOT EXISTS `t_festival_agegroups` (
  `festivalId` int(11) NOT NULL,
  `ageGroupId` int(11) NOT NULL,
  KEY `FK_a95wrgsba59cl0jnejg6vwyie` (`ageGroupId`),
  KEY `FK_j3p2nefcd34pd9h871vbjenma` (`festivalId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `t_festival_journalist`
--

CREATE TABLE IF NOT EXISTS `t_festival_journalist` (
  `festivalId` int(11) NOT NULL,
  `journalistId` int(11) NOT NULL,
  KEY `FK_3rh98cnpw1xj61lokba8rmv2h` (`journalistId`),
  KEY `FK_2t88e3eescri70ahjkao0g4h1` (`festivalId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `t_festival_salespoints`
--

CREATE TABLE IF NOT EXISTS `t_festival_salespoints` (
  `festivalId` int(11) NOT NULL,
  `salesPointId` int(11) NOT NULL,
  KEY `FK_lath23krvupxkblwrk7tygyp9` (`salesPointId`),
  KEY `FK_9qff0sca34rhyycuwy6jeyhbb` (`festivalId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `t_journalist`
--

CREATE TABLE IF NOT EXISTS `t_journalist` (
  `journalistId` int(11) NOT NULL AUTO_INCREMENT,
  `company` varchar(255) DEFAULT NULL,
  `firstName` varchar(255) DEFAULT NULL,
  `lastName` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`journalistId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `t_journalist_artistinfo`
--

CREATE TABLE IF NOT EXISTS `t_journalist_artistinfo` (
  `journalistId` int(11) NOT NULL,
  `artistInfoId` int(11) NOT NULL,
  KEY `FK_g80vba42dtrbqjdfof81tfy93` (`artistInfoId`),
  KEY `FK_73ors77tcxfnt73d80jt0l7qv` (`journalistId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `t_journalist_ticket`
--

CREATE TABLE IF NOT EXISTS `t_journalist_ticket` (
  `journalistId` int(11) NOT NULL,
  `ticketId` int(11) NOT NULL,
  UNIQUE KEY `UK_mlto0y1vatgrddemyfxcc6bsy` (`ticketId`),
  KEY `FK_mlto0y1vatgrddemyfxcc6bsy` (`ticketId`),
  KEY `FK_nw11jpn8a1at2dy0hp2erpj9t` (`journalistId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `t_location`
--

CREATE TABLE IF NOT EXISTS `t_location` (
  `locationId` int(11) NOT NULL AUTO_INCREMENT,
  `address` varchar(255) DEFAULT NULL,
  `maxVisitors` int(11) DEFAULT NULL,
  PRIMARY KEY (`locationId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `t_need`
--

CREATE TABLE IF NOT EXISTS `t_need` (
  `needId` int(11) NOT NULL AUTO_INCREMENT,
  `amount` int(11) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`needId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=54 ;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `t_performance`
--

CREATE TABLE IF NOT EXISTS `t_performance` (
  `performanceId` int(11) NOT NULL AUTO_INCREMENT,
  `endTime` datetime DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `startTime` datetime DEFAULT NULL,
  `artistId` int(11) DEFAULT NULL,
  `facilityId` int(11) DEFAULT NULL,
  `festivalId` int(11) DEFAULT NULL,
  `festivalDayId` int(11) DEFAULT NULL,
  `soundCheckId` int(11) DEFAULT NULL,
  PRIMARY KEY (`performanceId`),
  KEY `FK_p8q5dk2d4ynolybuk23g5dj7c` (`artistId`),
  KEY `FK_te8vs20atlhfjqh59gjkt3osq` (`facilityId`),
  KEY `FK_cqmgn7f3iqwjk76s49r9nuprr` (`festivalId`),
  KEY `FK_l56smao5t56gymkeiilpp0a7u` (`festivalDayId`),
  KEY `FK_oful5r6b1vebdokasdng998dh` (`soundCheckId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=47 ;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `t_performance_need`
--

CREATE TABLE IF NOT EXISTS `t_performance_need` (
  `performanceId` int(11) NOT NULL,
  `needId` int(11) NOT NULL,
  KEY `FK_5ncw8w7pgbkrpcav4ytuiblbc` (`needId`),
  KEY `FK_828mop5td4x0uboje9b98d3oi` (`performanceId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `t_performance_tracking`
--

CREATE TABLE IF NOT EXISTS `t_performance_tracking` (
  `performanceId` int(11) NOT NULL,
  `numberOfTrackings` int(11) DEFAULT NULL,
  UNIQUE KEY `performanceId` (`performanceId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `t_salespoint`
--

CREATE TABLE IF NOT EXISTS `t_salespoint` (
  `SalesPointType` varchar(31) NOT NULL,
  `salesPointId` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `website` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`salesPointId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `t_song`
--

CREATE TABLE IF NOT EXISTS `t_song` (
  `songId` int(11) NOT NULL AUTO_INCREMENT,
  `duration` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `artistId` int(11) DEFAULT NULL,
  PRIMARY KEY (`songId`),
  KEY `FK_5cn57k8mj32cox4vmcj8hecy2` (`artistId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=136 ;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `t_soundcheck`
--

CREATE TABLE IF NOT EXISTS `t_soundcheck` (
  `soundCheckId` int(11) NOT NULL AUTO_INCREMENT,
  `endTime` datetime DEFAULT NULL,
  `startTime` datetime DEFAULT NULL,
  PRIMARY KEY (`soundCheckId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=47 ;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `t_task`
--

CREATE TABLE IF NOT EXISTS `t_task` (
  `taskId` int(11) NOT NULL AUTO_INCREMENT,
  `description` longtext,
  `festivalId` int(11) DEFAULT NULL,
  `festivalDayId` int(11) DEFAULT NULL,
  PRIMARY KEY (`taskId`),
  KEY `FK_8ofsdotfkbaonq78mg6wxgdc3` (`festivalId`),
  KEY `FK_qbi0xj8aw5xhun2ceou4w8jet` (`festivalDayId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `t_ticket`
--

CREATE TABLE IF NOT EXISTS `t_ticket` (
  `ticketId` int(11) NOT NULL AUTO_INCREMENT,
  `age` int(11) DEFAULT NULL,
  `ageGroupName` varchar(255) DEFAULT NULL,
  `barcode` varchar(255) DEFAULT NULL,
  `ageGroupId` int(11) DEFAULT NULL,
  `festivalId` int(11) DEFAULT NULL,
  `salesPointId` int(11) DEFAULT NULL,
  `ticketTypeId` int(11) DEFAULT NULL,
  `trackingBraceletId` int(11) DEFAULT NULL,
  PRIMARY KEY (`ticketId`),
  UNIQUE KEY `UK_2o4rf7bn3gda0j61642wsrkrp` (`barcode`),
  KEY `FK_gsciled6kg01wm04mndy9se21` (`ageGroupId`),
  KEY `FK_ps3rn20dqahlqnu81svd8d0er` (`festivalId`),
  KEY `FK_den708sauq6kwa7ncuxk9g7s2` (`salesPointId`),
  KEY `FK_r98ot0xf4xax4wbfile5vy9b` (`ticketTypeId`),
  KEY `FK_ad8daexignh4kmwn6xe90ldn0` (`trackingBraceletId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `t_tickettype`
--

CREATE TABLE IF NOT EXISTS `t_tickettype` (
  `ticketTypeId` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `price` double DEFAULT NULL,
  `festivalId` int(11) DEFAULT NULL,
  PRIMARY KEY (`ticketTypeId`),
  KEY `FK_36ddeu3ujc1gq38yljmunqx09` (`festivalId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `t_tickettype_facilitytype`
--

CREATE TABLE IF NOT EXISTS `t_tickettype_facilitytype` (
  `ticketTypeId` int(11) NOT NULL,
  `facilityTypeId` int(11) NOT NULL,
  KEY `FK_6eirev2grnyq0wwjy0abgncss` (`facilityTypeId`),
  KEY `FK_7rjvx8cujm8tbq9ujl9p81avf` (`ticketTypeId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `t_tickettype_festivalday`
--

CREATE TABLE IF NOT EXISTS `t_tickettype_festivalday` (
  `ticketTypeId` int(11) NOT NULL,
  `festivalDayId` int(11) NOT NULL,
  KEY `FK_rm4ygebh7yqe316hvxt55rfmb` (`festivalDayId`),
  KEY `FK_8yh98ce836dro87k9dca47lqg` (`ticketTypeId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `t_tracking`
--

CREATE TABLE IF NOT EXISTS `t_tracking` (
  `trackingId` int(11) NOT NULL AUTO_INCREMENT,
  `time` datetime DEFAULT NULL,
  `type` tinyint(1) NOT NULL,
  `facilityId` int(11) DEFAULT NULL,
  `festivalId` int(11) DEFAULT NULL,
  `trackingBraceletId` int(11) DEFAULT NULL,
  PRIMARY KEY (`trackingId`),
  KEY `FK_rwr2fk3ifufbr23ky6b2b314i` (`facilityId`),
  KEY `FK_47caw4oaqons1n6rkmy0dki3e` (`festivalId`),
  KEY `FK_1878dwcapvxmnqbr24u95t4py` (`trackingBraceletId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=214 ;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `t_trackingbracelet`
--

CREATE TABLE IF NOT EXISTS `t_trackingbracelet` (
  `braceletId` int(11) NOT NULL AUTO_INCREMENT,
  `serialNo` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`braceletId`),
  UNIQUE KEY `UK_f2regrntv34obl0mouwokjk3x` (`serialNo`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=67 ;

--
-- Beperkingen voor gedumpte tabellen
--

--
-- Beperkingen voor tabel `t_artist`
--
ALTER TABLE `t_artist`
  ADD CONSTRAINT `FK_k81dfixb9m3r1l686d9l5d4hh` FOREIGN KEY (`facilityId`) REFERENCES `t_facility` (`facilityId`),
  ADD CONSTRAINT `FK_576rbi5n6f19t66ek0e1a9hm8` FOREIGN KEY (`artistInfoId`) REFERENCES `t_artistinfo` (`artistInfoId`);

--
-- Beperkingen voor tabel `t_artistinfo`
--
ALTER TABLE `t_artistinfo`
  ADD CONSTRAINT `FK_kqn2mamn94343j8sefjbpoa9a` FOREIGN KEY (`festivalId`) REFERENCES `t_festival` (`festivalId`);

--
-- Beperkingen voor tabel `t_employee`
--
ALTER TABLE `t_employee`
  ADD CONSTRAINT `FK_axqg68nmgo3w8p03993c0bgv8` FOREIGN KEY (`festivalId`) REFERENCES `t_festival` (`festivalId`);

--
-- Beperkingen voor tabel `t_employee_task`
--
ALTER TABLE `t_employee_task`
  ADD CONSTRAINT `FK_nrenv6wv4e5st3c64ismsngk3` FOREIGN KEY (`employeeId`) REFERENCES `t_employee` (`employeeId`),
  ADD CONSTRAINT `FK_81sd9fumstbw8ae52lk5491xs` FOREIGN KEY (`taskId`) REFERENCES `t_task` (`taskId`);

--
-- Beperkingen voor tabel `t_facility`
--
ALTER TABLE `t_facility`
  ADD CONSTRAINT `FK_1uxguwea9fbhj8u64eu4k2y5e` FOREIGN KEY (`festivalId`) REFERENCES `t_festival` (`festivalId`),
  ADD CONSTRAINT `FK_qrqnr96y8qpc08xw0228aihyk` FOREIGN KEY (`facilityTypeId`) REFERENCES `t_facilitytype` (`facilityTypeId`),
  ADD CONSTRAINT `FK_slofsoqfmpisrsu1emhv6bssr` FOREIGN KEY (`subFacilityId`) REFERENCES `t_facility` (`facilityId`);

--
-- Beperkingen voor tabel `t_facilitytype`
--
ALTER TABLE `t_facilitytype`
  ADD CONSTRAINT `FK_smkrcmo5h6h5js7bhmjakq7oi` FOREIGN KEY (`festivalId`) REFERENCES `t_festival` (`festivalId`);

--
-- Beperkingen voor tabel `t_facility_visittimes`
--
ALTER TABLE `t_facility_visittimes`
  ADD CONSTRAINT `FK_q3k10u662686ih6qu5ucvhc9i` FOREIGN KEY (`Facility_facilityId`) REFERENCES `t_facility` (`facilityId`);

--
-- Beperkingen voor tabel `t_festival`
--
ALTER TABLE `t_festival`
  ADD CONSTRAINT `FK_mdqyn08iijgjtvixvj1hgf0yc` FOREIGN KEY (`locationId`) REFERENCES `t_location` (`locationId`);

--
-- Beperkingen voor tabel `t_festivalday`
--
ALTER TABLE `t_festivalday`
  ADD CONSTRAINT `FK_1c64ckhitpv6u66a2rsjmplls` FOREIGN KEY (`festivalId`) REFERENCES `t_festival` (`festivalId`);

--
-- Beperkingen voor tabel `t_festival_agegroups`
--
ALTER TABLE `t_festival_agegroups`
  ADD CONSTRAINT `FK_j3p2nefcd34pd9h871vbjenma` FOREIGN KEY (`festivalId`) REFERENCES `t_festival` (`festivalId`),
  ADD CONSTRAINT `FK_a95wrgsba59cl0jnejg6vwyie` FOREIGN KEY (`ageGroupId`) REFERENCES `t_agegroup` (`ageGroupId`);

--
-- Beperkingen voor tabel `t_festival_journalist`
--
ALTER TABLE `t_festival_journalist`
  ADD CONSTRAINT `FK_2t88e3eescri70ahjkao0g4h1` FOREIGN KEY (`festivalId`) REFERENCES `t_festival` (`festivalId`),
  ADD CONSTRAINT `FK_3rh98cnpw1xj61lokba8rmv2h` FOREIGN KEY (`journalistId`) REFERENCES `t_journalist` (`journalistId`);

--
-- Beperkingen voor tabel `t_festival_salespoints`
--
ALTER TABLE `t_festival_salespoints`
  ADD CONSTRAINT `FK_9qff0sca34rhyycuwy6jeyhbb` FOREIGN KEY (`festivalId`) REFERENCES `t_festival` (`festivalId`),
  ADD CONSTRAINT `FK_lath23krvupxkblwrk7tygyp9` FOREIGN KEY (`salesPointId`) REFERENCES `t_salespoint` (`salesPointId`);

--
-- Beperkingen voor tabel `t_journalist_artistinfo`
--
ALTER TABLE `t_journalist_artistinfo`
  ADD CONSTRAINT `FK_73ors77tcxfnt73d80jt0l7qv` FOREIGN KEY (`journalistId`) REFERENCES `t_journalist` (`journalistId`),
  ADD CONSTRAINT `FK_g80vba42dtrbqjdfof81tfy93` FOREIGN KEY (`artistInfoId`) REFERENCES `t_artistinfo` (`artistInfoId`);

--
-- Beperkingen voor tabel `t_journalist_ticket`
--
ALTER TABLE `t_journalist_ticket`
  ADD CONSTRAINT `FK_nw11jpn8a1at2dy0hp2erpj9t` FOREIGN KEY (`journalistId`) REFERENCES `t_journalist` (`journalistId`),
  ADD CONSTRAINT `FK_mlto0y1vatgrddemyfxcc6bsy` FOREIGN KEY (`ticketId`) REFERENCES `t_ticket` (`ticketId`);

--
-- Beperkingen voor tabel `t_performance`
--
ALTER TABLE `t_performance`
  ADD CONSTRAINT `FK_oful5r6b1vebdokasdng998dh` FOREIGN KEY (`soundCheckId`) REFERENCES `t_soundcheck` (`soundCheckId`),
  ADD CONSTRAINT `FK_cqmgn7f3iqwjk76s49r9nuprr` FOREIGN KEY (`festivalId`) REFERENCES `t_festival` (`festivalId`),
  ADD CONSTRAINT `FK_l56smao5t56gymkeiilpp0a7u` FOREIGN KEY (`festivalDayId`) REFERENCES `t_festivalday` (`festivalDayId`),
  ADD CONSTRAINT `FK_p8q5dk2d4ynolybuk23g5dj7c` FOREIGN KEY (`artistId`) REFERENCES `t_artist` (`artistId`),
  ADD CONSTRAINT `FK_te8vs20atlhfjqh59gjkt3osq` FOREIGN KEY (`facilityId`) REFERENCES `t_facility` (`facilityId`);

--
-- Beperkingen voor tabel `t_performance_need`
--
ALTER TABLE `t_performance_need`
  ADD CONSTRAINT `FK_828mop5td4x0uboje9b98d3oi` FOREIGN KEY (`performanceId`) REFERENCES `t_performance` (`performanceId`),
  ADD CONSTRAINT `FK_5ncw8w7pgbkrpcav4ytuiblbc` FOREIGN KEY (`needId`) REFERENCES `t_need` (`needId`);

--
-- Beperkingen voor tabel `t_song`
--
ALTER TABLE `t_song`
  ADD CONSTRAINT `FK_5cn57k8mj32cox4vmcj8hecy2` FOREIGN KEY (`artistId`) REFERENCES `t_artist` (`artistId`);

--
-- Beperkingen voor tabel `t_task`
--
ALTER TABLE `t_task`
  ADD CONSTRAINT `FK_qbi0xj8aw5xhun2ceou4w8jet` FOREIGN KEY (`festivalDayId`) REFERENCES `t_festivalday` (`festivalDayId`),
  ADD CONSTRAINT `FK_8ofsdotfkbaonq78mg6wxgdc3` FOREIGN KEY (`festivalId`) REFERENCES `t_festival` (`festivalId`);

--
-- Beperkingen voor tabel `t_ticket`
--
ALTER TABLE `t_ticket`
  ADD CONSTRAINT `FK_ad8daexignh4kmwn6xe90ldn0` FOREIGN KEY (`trackingBraceletId`) REFERENCES `t_trackingbracelet` (`braceletId`),
  ADD CONSTRAINT `FK_den708sauq6kwa7ncuxk9g7s2` FOREIGN KEY (`salesPointId`) REFERENCES `t_salespoint` (`salesPointId`),
  ADD CONSTRAINT `FK_gsciled6kg01wm04mndy9se21` FOREIGN KEY (`ageGroupId`) REFERENCES `t_agegroup` (`ageGroupId`),
  ADD CONSTRAINT `FK_ps3rn20dqahlqnu81svd8d0er` FOREIGN KEY (`festivalId`) REFERENCES `t_festival` (`festivalId`),
  ADD CONSTRAINT `FK_r98ot0xf4xax4wbfile5vy9b` FOREIGN KEY (`ticketTypeId`) REFERENCES `t_tickettype` (`ticketTypeId`);

--
-- Beperkingen voor tabel `t_tickettype`
--
ALTER TABLE `t_tickettype`
  ADD CONSTRAINT `FK_36ddeu3ujc1gq38yljmunqx09` FOREIGN KEY (`festivalId`) REFERENCES `t_festival` (`festivalId`);

--
-- Beperkingen voor tabel `t_tickettype_facilitytype`
--
ALTER TABLE `t_tickettype_facilitytype`
  ADD CONSTRAINT `FK_7rjvx8cujm8tbq9ujl9p81avf` FOREIGN KEY (`ticketTypeId`) REFERENCES `t_tickettype` (`ticketTypeId`),
  ADD CONSTRAINT `FK_6eirev2grnyq0wwjy0abgncss` FOREIGN KEY (`facilityTypeId`) REFERENCES `t_facilitytype` (`facilityTypeId`);

--
-- Beperkingen voor tabel `t_tickettype_festivalday`
--
ALTER TABLE `t_tickettype_festivalday`
  ADD CONSTRAINT `FK_8yh98ce836dro87k9dca47lqg` FOREIGN KEY (`ticketTypeId`) REFERENCES `t_tickettype` (`ticketTypeId`),
  ADD CONSTRAINT `FK_rm4ygebh7yqe316hvxt55rfmb` FOREIGN KEY (`festivalDayId`) REFERENCES `t_festivalday` (`festivalDayId`);

--
-- Beperkingen voor tabel `t_tracking`
--
ALTER TABLE `t_tracking`
  ADD CONSTRAINT `FK_1878dwcapvxmnqbr24u95t4py` FOREIGN KEY (`trackingBraceletId`) REFERENCES `t_trackingbracelet` (`braceletId`),
  ADD CONSTRAINT `FK_47caw4oaqons1n6rkmy0dki3e` FOREIGN KEY (`festivalId`) REFERENCES `t_festival` (`festivalId`),
  ADD CONSTRAINT `FK_rwr2fk3ifufbr23ky6b2b314i` FOREIGN KEY (`facilityId`) REFERENCES `t_facility` (`facilityId`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

xquery version "1.0";
<Performances> 
{
for $date in /PressRelease/Festival/Date
	let $startDate := /PressRelease/Festival/@startDate
	let $artist := distinct-values($date/Podium/Artist/@name)
	let $dateDiff := fn:days-from-duration(xs:dayTimeDuration($date/@value -  $startDate)) 
	where $dateDiff = 2
return 	
		for $time at $pos in distinct-values($date/Podium/Artist/Performance/@startTime)
		order by $time
		return
			 <Performance artist="{$artist[$pos]}">
				 <Time>{$time}</Time>
			 </Performance>
 }
</Performances>
xquery version "1.0";
<Artists> 
{
for $dates in /PressRelease/Festival/Date
let $startDate := /PressRelease/Festival/@startDate
return 

		for $date in $dates
			let $dateDiff := fn:days-from-duration(xs:dayTimeDuration($date/@value -  $startDate)) 
		where  $dateDiff = 1 
		return 
			for $artist in $date/Podium/Artist
			where $artist/AvailableAt/@facility = "Dressing Room"
			return
	<Artist name="{$artist/@name}">
		<AvailableAt time = "{ $artist/AvailableAt/@time }" />
	</Artist>
}
</Artists>
USE project_festival;
SELECT 
(SELECT count(*) FROM t_ticket tic, t_salespoint sp WHERE tic.age BETWEEN 16 AND 17 AND sp.SalesPointType = 'WebSale' AND tic.salesPointId = sp.salesPointId) '[16-18]', 
(SELECT count(*) FROM t_ticket tic, t_salespoint sp WHERE tic.age BETWEEN 18 AND 20 AND sp.SalesPointType = 'WebSale' AND tic.salesPointId = sp.salesPointId) '[18-21]', 
(SELECT count(*) FROM t_ticket tic, t_salespoint sp WHERE tic.age BETWEEN 21 AND 25 AND sp.SalesPointType = 'WebSale' AND tic.salesPointId = sp.salesPointId) '[21-25]', 
(SELECT count(*) FROM t_ticket tic, t_salespoint sp WHERE tic.age > 25 AND sp.SalesPointType = 'WebSale' AND tic.salesPointId = sp.salesPointId) '[> 25]'
FROM dual;
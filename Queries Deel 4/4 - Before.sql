USE project_festival;
SELECT trac1.facilityId,AVG(time_to_sec(TIMEDIFF(TIME(trac2.time),TIME(trac1.time)))) gem_per_fac
FROM t_tracking trac1, t_tracking trac2
WHERE trac1.facilityId = trac2.facilityId AND trac1.type=1 AND trac2.type=0 AND trac1.trackingBraceletId = trac2.trackingBraceletId AND trac1.time < trac2.time
GROUP BY trac1.facilityId
ORDER BY 1;
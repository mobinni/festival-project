USE project_festival;
SELECT fest.name, fest.totalSold
FROM t_festival fest;

/*
	We hebben in Festival een veld 'totalSold' toegevoegd, die de totale omzet de ticketverkoop van alle SalesPoints bijhoudt.
	Elke keer er een ticket gekocht wordt, wordt de prijs daarvan bij dit veld opgeteld.
*/
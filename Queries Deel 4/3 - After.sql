SELECT  tic.ageGroupName, count(*)
FROM t_ticket tic
GROUP BY tic.ageGroupName;

/*
	We hebben een entiteit AgeGroup moeten toevoegen aangezien we deze niet bijhielden.
	Een een eerste query (die werkte met joins) deed er ongeveer 1.25 seconden over.
	Daarom hebben we gedenormaliseerd en de naam van de AgeGroup bij het ticket gestoken.
/*
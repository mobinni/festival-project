USE project_festival;
SELECT f.name, sum(tType.price) FROM t_ticket t, t_tickettype tType, t_festival f
WHERE t.ticketTypeId = tType.ticketTypeId AND f.festivalId = tType.festivalId
GROUP BY f.name;
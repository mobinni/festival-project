SELECT per.name, pt.numberOfTrackings
FROM t_performance_tracking pt, t_performance per
WHERE per.performanceId = pt.performanceId
ORDER BY 2 DESC
LIMIT 10;

/*
We hebben een tabel 't_performance_tracking' met de kolommen 'performanceId' (met unique constraint) en 'numberOfTrackings'
Elke keer een tracking met als type 'IN' ontvangen wordt, zal de waarde 'numberOfTrackings', van de overeenkomstige performanceId,  met 1 verhoogd worden.
*/
USE project_festival;
SELECT fest.name festivalName,festd.date, perf.name performanceName, perf.performanceId performanceId,fac.name Stage,perf.startTime,perf.endTime, (SELECT count(*) FROM t_tracking trac WHERE trac.facilityId = fac.subFacilityId AND trac.type = 1 AND trac.time > perf.startTime AND trac.time < perf.endTime) '# people'
FROM t_performance perf, t_facility fac, t_festival fest, t_festivalday festd
WHERE fac.facilityId = perf.facilityId AND fest.festivalId = perf.festivalId AND festd.festivalDayId = perf.festivalDayId
ORDER BY 8 DESC
LIMIT 10; 
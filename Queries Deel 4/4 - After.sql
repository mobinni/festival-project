USE project_festival;
SELECT facvis.Facility_facilityId, avg(facvis.visitTimes) 'Avg time (sec)'
FROM t_facility_visittimes facvis
GROUP BY facvis.Facility_facilityId;

/*
Elke keer een tracking met als type 'OUT' ontvangen wordt, zoeken we de overeenkomstige tracking van het type 'IN'.
Het verschil in tijd (dit is dus de tijd die een persoon in de zone heeft doorgebracht) wordt samen met de Id van de zone in een tabel gestoken.
Met deze query halen we per zone de gemiddelde tijden op.
*/
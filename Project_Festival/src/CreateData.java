import helper.DateHelper;
import helper.NameGenerator;
import model.*;
import org.hibernate.Session;
import org.hibernate.Transaction;
import persistency.HibernateUtil;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

/**
 * Created with IntelliJ IDEA.
 * User: Thomas
 * Date: 25/10/13
 * Time: 20:54
 * To change this template use File | Settings | File Templates.
 */
public class CreateData {
    private final static NameGenerator NAME_GENERATOR = new NameGenerator();
    private final static Random RANDOM = new Random();
    private static long time;

    public static void main(String[] args) throws ParseException {
        time = System.currentTimeMillis();
        Session session = HibernateUtil.getSessionFactory().openSession();

        session.createSQLQuery("DROP TABLE IF EXISTS t_performance_tracking;").executeUpdate();
        session.createSQLQuery("CREATE TABLE t_performance_tracking (performanceId int NOT NULL REFERENCES t_performance(performanceId), numberOfTrackings int, CONSTRAINT UNIQUE (performanceId));").executeUpdate();

        SalesPoint websiteSalesPoint = new WebSale("WebsiteSalesPoint", "http://WebsiteSalesPoint.com");
        AgeGroup ageGroup1 = new AgeGroup(16, 18);
        AgeGroup ageGroup2 = new AgeGroup(18, 21);
        AgeGroup ageGroup3 = new AgeGroup(21, 25);
        AgeGroup ageGroup4 = new AgeGroup(25, 100);

        List<Artist> allArtists = new ArrayList<Artist>();
        for (int i = 0; i < 160; i++) {
            String name = NAME_GENERATOR.getName();
            Artist artist = new Artist(
                    String.format("artist_%s", name),
                    String.format("manager_%s", name)
            );
            ArtistInfo info = new ArtistInfo(String.format("%s biography met een hele lap tekste erbij", artist.getName()), RANDOM.nextBoolean());
            artist.setArtistInfo(info);
            for (int j = 0; j < 5; j++) {
                Song song = new Song(NAME_GENERATOR.getName(), RANDOM.nextInt(61) + 150);
                artist.addSong(song);


            }
            allArtists.add(artist);
        }

        List<Journalist> allJournalists = new ArrayList<Journalist>();
        for(int i = 0; i < 50; i++){
            Journalist journalist = new Journalist(
                    String.format("firstName_%s", NAME_GENERATOR.getName()),
                    String.format("lastName_%s", NAME_GENERATOR.getName()),
                    String.format("company_%s", NAME_GENERATOR.getName())
            );
            allJournalists.add(journalist);
        }

        List<Employee> allEmployees = new ArrayList<Employee>();
        for(int i = 0; i < 100; i++){
            Employee employee = new Employee(
                    NAME_GENERATOR.getName(), NAME_GENERATOR.getName(),
                    String.format("=324651%d", RANDOM.nextInt(1000000)),
                    String.format("Rue de %s %d Antwerpen", NAME_GENERATOR.getName(), RANDOM.nextInt(2500))
            );
            allEmployees.add(employee);
        }
        for (int f = 0; f < 10; f++) {
            Transaction tx = session.beginTransaction();
        /*
        Festival
         */
            Festival festival = new Festival(String.format("festival_%s_%d", NAME_GENERATOR.getName(), f), DateHelper.createTime(10, 0), DateHelper.createTime(18, 0));

         /*
         AgeGroups
          */
            festival.addAgeGroup(ageGroup1);
            festival.addAgeGroup(ageGroup2);
            festival.addAgeGroup(ageGroup3);
            festival.addAgeGroup(ageGroup4);
        /*
        Location
         */
            Location location = new Location(8000, String.format("Rue de %s %d Antwerpen", NAME_GENERATOR.getName(), RANDOM.nextInt(2500)));
            festival.setLocation(location);

        /*
        FestivalDays
         */
            List<FestivalDay> festivalDays = new ArrayList<FestivalDay>();
            for (int i = 0; i < RANDOM.nextInt(3) + 2; i++) {
                festivalDays.add(new FestivalDay(DateHelper.createDate(20 + i, f + 1, 2013)));
            }
            festival.setFestivalDays(festivalDays);

        /*
        FacilityType
         */
            FacilityType stageFacilityType = new FacilityType("Stage");
            FacilityType stageGuestZone = new FacilityType("Stage Guest Zone");
            FacilityType toiletFacilityType = new FacilityType("Toilet");
            FacilityType dressingFacilityType = new FacilityType("Dressing Room");
            FacilityType vipRoomFacilityType = new FacilityType("Vip Room");

            festival.addFacilityType(stageFacilityType);
            festival.addFacilityType(stageGuestZone);
            festival.addFacilityType(toiletFacilityType);
            festival.addFacilityType(dressingFacilityType);
            festival.addFacilityType(vipRoomFacilityType);

        /*
        Facilities
         */
            List<Facility> allFacilities = new ArrayList<Facility>();
            List<Facility> stageFacilities = new ArrayList<Facility>();
            for (int i = 0; i < RANDOM.nextInt(3) + 2; i++) {
                Facility facility = new Facility(
                        String.format("facility_stage_%s", NAME_GENERATOR.getName()),
                        String.format("RGB(%d,%d,%d)", RANDOM.nextInt(256), RANDOM.nextInt(256), RANDOM.nextInt(256))
                );
                facility.setFacilityType(stageFacilityType);
                stageFacilities.add(facility);

                Facility guestZone = new Facility(String.format("guestZone_%s", facility.getName()), String.format("RGB(%d,%d,%d)", RANDOM.nextInt(256), RANDOM.nextInt(256), RANDOM.nextInt(256)));
                guestZone.setFacilityType(stageGuestZone);
                facility.setFacility(guestZone);
                allFacilities.add(guestZone);
            }


            for (int i = 0; i < RANDOM.nextInt(3) + 3; i++) {
                Facility facility = new Facility(
                        String.format("facility_toilet_%s", NAME_GENERATOR.getName()),
                        String.format("RGB(%d,%d,%d)", RANDOM.nextInt(256), RANDOM.nextInt(256), RANDOM.nextInt(256))
                );
                facility.setFacilityType(toiletFacilityType);
                allFacilities.add(facility);

                facility = new Facility(
                        String.format("facility_vip_%s", NAME_GENERATOR.getName()),
                        String.format("RGB(%d,%d,%d)", RANDOM.nextInt(256), RANDOM.nextInt(256), RANDOM.nextInt(256))
                );
                facility.setFacilityType(vipRoomFacilityType);
                allFacilities.add(facility);
            }

        /*
        Performances + Needs + SoundCheck
         */
            List<Performance> performances = new ArrayList<Performance>();
            List<Need> needs = new ArrayList<Need>();
            for (FestivalDay day : festivalDays) {
                for (Facility stage : stageFacilities) {
                    for (int i = 0; i < 8; i++) {
                        Performance performance = new Performance(NAME_GENERATOR.getName(), DateHelper.createTime(10 + i, 5), DateHelper.createTime(10 + i, 59));
                        performance.setFestivalDay(day);
                        performance.setFacility(stage);
                        performances.add(performance);

                        SoundCheck soundCheck = new SoundCheck(DateHelper.createTime(10 + i, 0), DateHelper.createTime(10 + i, 5));
                        performance.setSoundCheck(soundCheck);

                        Need need = new Need(NAME_GENERATOR.getName(), RANDOM.nextInt(8) + 1);
                        performance.addNeed(need);
                        if (needs.size() > 1 && RANDOM.nextInt(4) == 2) {
                            Need n = needs.get(RANDOM.nextInt(needs.size() - 1));
                            if (n != null) performance.addNeed(n);
                        }
                        needs.add(need);
                    }
                }
            }
            festival.setPerformances(performances);

        /*
        Artists + Songs + ArtistInfos
         */
            List<ArtistInfo> localArtistInfos = new ArrayList<ArtistInfo>();
            for (int i = 0; i < performances.size(); i++) {
                Artist randomArtist = allArtists.get(RANDOM.nextInt(allArtists.size()));
                localArtistInfos.add(randomArtist.getArtistInfo());

                Facility facility = new Facility(
                        String.format("facility_dressing_%s", randomArtist.getName()),
                        String.format("RGB(%d,%d,%d)", RANDOM.nextInt(256), RANDOM.nextInt(256), RANDOM.nextInt(256))
                );
                facility.setFacilityType(dressingFacilityType);
                randomArtist.setFacility(facility);
                allFacilities.add(facility);
                randomArtist.addPerformance(performances.get(i));
            }

            festival.setArtistInfos(localArtistInfos);

            allFacilities.addAll(stageFacilities);
            festival.setFacilities(allFacilities);

        /*
        Journalists
         */
            List<Journalist> localJournalists = new ArrayList<Journalist>();
            for (int i = 0; i < .3 * performances.size(); i++) {
                Journalist randomJournalist = allJournalists.get(RANDOM.nextInt(allJournalists.size()));
                randomJournalist.setArtistInfos(localArtistInfos);
                localJournalists.add(randomJournalist);
            }
            festival.setJournalists(localJournalists);

        /*
        Tasks + Employees
         */
            List<Task> tasks = new ArrayList<Task>();
            for (int i = 0; i < allFacilities.size() * 3; i++) {
                Task task = new Task(String.format("Er moet iets gebeuren aan de %s", NAME_GENERATOR.getName()));
                task.setFestivalDay(festivalDays.get(RANDOM.nextInt(festivalDays.size())));
                tasks.add(task);
            }
            festival.setTasks(tasks);

            List<Employee> localEmployees = new ArrayList<Employee>();
            for(int i = 0; i < .3 * tasks.size(); i++){
                localEmployees.add(allEmployees.get(RANDOM.nextInt(allEmployees.size())));
            }
            festival.setEmployees(localEmployees);

            for (int i = 0; i < tasks.size(); i++) {
                tasks.get(i).addEmployee(localEmployees.get(RANDOM.nextInt(localEmployees.size())));
            }

        /*
        TicketTypes
         */
            List<TicketType> dayTicketTypes = new ArrayList<TicketType>();
            for (FestivalDay day : festivalDays) {
                TicketType ticketType = new TicketType("DayTicketType", 75.0);
                ticketType.addFestivalDay(day);
                ticketType.addFacilityType(stageGuestZone);
                ticketType.addFacilityType(toiletFacilityType);
                dayTicketTypes.add(ticketType);
            }
            TicketType combiTicket = new TicketType("CombiTicketType", festivalDays.size() * 70.0);
            combiTicket.addFacilityType(stageGuestZone);
            combiTicket.addFacilityType(toiletFacilityType);
            for (FestivalDay festivalDay : festivalDays) {
                combiTicket.addFestivalDay(festivalDay);
            }
            List<TicketType> vipTicketTypes = new ArrayList<TicketType>();
            for (FestivalDay day : festivalDays) {
                TicketType ticketType = new TicketType("VipTicketType", 150.0);
                ticketType.addFestivalDay(day);
                ticketType.addFacilityType(stageGuestZone);
                ticketType.addFacilityType(toiletFacilityType);
                ticketType.addFacilityType(vipRoomFacilityType);
                vipTicketTypes.add(ticketType);
            }
            List<TicketType> journalistTicketTypes = new ArrayList<TicketType>();
            for (FestivalDay day : festivalDays) {
                TicketType ticketType = new TicketType("JournalistTicketType", 100.0);
                ticketType.addFestivalDay(day);
                ticketType.addFacilityType(stageGuestZone);
                ticketType.addFacilityType(toiletFacilityType);
                ticketType.addFacilityType(vipRoomFacilityType);
                ticketType.addFacilityType(stageFacilityType);
                journalistTicketTypes.add(ticketType);
            }
            festival.setTicketTypes(dayTicketTypes);
            festival.addTicketType(combiTicket);
            festival.setTicketTypes(vipTicketTypes);
            festival.setTicketTypes(journalistTicketTypes);

            //JournalistTickets
            List<Ticket> journalistTickets = new ArrayList<Ticket>();
            for (Journalist journalist : localJournalists) {
                List<Integer> types = new ArrayList<Integer>();
                for (int i = 0; i < RANDOM.nextInt(journalistTicketTypes.size()) + 1; i++) {
                    Ticket ticket = new Ticket();
                    int random = RANDOM.nextInt(journalistTicketTypes.size());
                    if (types.contains(random)) continue;
                    int age = RANDOM.nextInt(85) + 16;
                    if (age >= 16 && age < 18) {
                        ticket.setAgeGroup(ageGroup1);
                    } else if (age >= 18 && age < 21) {
                        ticket.setAgeGroup(ageGroup2);
                    } else if (age >= 21 && age < 25) {
                        ticket.setAgeGroup(ageGroup3);
                    } else {
                        ticket.setAgeGroup(ageGroup4);
                    }
                    ticket.setAge(age);
                    types.add(random);
                    ticket.setTicketType(journalistTicketTypes.get(random));
                    journalist.addTicket(ticket);
                    TrackingBracelet bracelet = new TrackingBracelet();
                    ticket.setTrackingBracelet(bracelet);
                    journalistTickets.add(ticket);
                }
            }

        /*
            SalesPoints
        */
            List<SalesPoint> entranceSalesPoints = new ArrayList<SalesPoint>();
            for (int i = 0; i < RANDOM.nextInt(4) + 1; i++) {
                SalesPoint entrance = new EntranceSale(String.format("Entrance%d", i));
                entranceSalesPoints.add(entrance);
            }
            List<SalesPoint> storeSalesPoints = new ArrayList<SalesPoint>();
            for (int i = 0; i < RANDOM.nextInt(10) + 1; i++) {
                SalesPoint store = new StoreSale(
                        String.format("Storee%d", i),
                        String.format("Rue de %s %d Antwerpen", NAME_GENERATOR.getName(), RANDOM.nextInt(2500))
                );
                storeSalesPoints.add(store);
            }

            festival.setSalesPoints(entranceSalesPoints);
            festival.setSalesPoints(storeSalesPoints);
            festival.addSalesPoint(websiteSalesPoint);

            for (Ticket ticket : journalistTickets) {
                ticket.setSalesPoint(websiteSalesPoint);
            }

        /*
        Tickets + Bracelets
         */
            double population = ((RANDOM.nextInt(16) + 75) / 100.0) * location.getMaxVisitors();
            List<Ticket> tickets = new ArrayList<Ticket>();
            for (int i = 0; i < .6 * population; i++) {
                Ticket ticket = new Ticket();
                int age = RANDOM.nextInt(85) + 16;
                if (age >= 16 && age < 18) {
                    ticket.setAgeGroup(ageGroup1);
                } else if (age >= 18 && age < 21) {
                    ticket.setAgeGroup(ageGroup2);
                } else if (age >= 21 && age < 25) {
                    ticket.setAgeGroup(ageGroup3);
                } else {
                    ticket.setAgeGroup(ageGroup4);
                }
                ticket.setAge(age);
                ticket.setSalesPoint(websiteSalesPoint);
                int ticketType = RANDOM.nextInt(10);
                if (ticketType < 7) {
                    ticket.setTicketType(dayTicketTypes.get(RANDOM.nextInt(dayTicketTypes.size())));
                } else if (ticketType < 9) {
                    ticket.setTicketType(combiTicket);
                } else {
                    ticket.setTicketType(vipTicketTypes.get(RANDOM.nextInt(vipTicketTypes.size())));
                }
                TrackingBracelet bracelet = new TrackingBracelet();
                ticket.setTrackingBracelet(bracelet);
                tickets.add(ticket);
            }
            for (int i = 0; i < .3 * population; i++) {
                Ticket ticket = new Ticket();
                int age = RANDOM.nextInt(85) + 16;
                if (age >= 16 && age < 18) {
                    ticket.setAgeGroup(ageGroup1);
                } else if (age >= 18 && age < 21) {
                    ticket.setAgeGroup(ageGroup2);
                } else if (age >= 21 && age < 25) {
                    ticket.setAgeGroup(ageGroup3);
                } else {
                    ticket.setAgeGroup(ageGroup4);
                }
                ticket.setAge(age);
                ticket.setSalesPoint(storeSalesPoints.get(RANDOM.nextInt(storeSalesPoints.size())));
                int ticketType = RANDOM.nextInt(10);
                if (ticketType < 7) {
                    ticket.setTicketType(dayTicketTypes.get(RANDOM.nextInt(dayTicketTypes.size())));
                } else if (ticketType < 9) {
                    ticket.setTicketType(combiTicket);
                } else {
                    ticket.setTicketType(vipTicketTypes.get(RANDOM.nextInt(vipTicketTypes.size())));
                }
                TrackingBracelet bracelet = new TrackingBracelet();
                ticket.setTrackingBracelet(bracelet);
                tickets.add(ticket);
            }
            for (int i = 0; i < .1 * population; i++) {
                Ticket ticket = new Ticket();
                int age = RANDOM.nextInt(85) + 16;
                if (age >= 16 && age < 18) {
                    ticket.setAgeGroup(ageGroup1);
                } else if (age >= 18 && age < 21) {
                    ticket.setAgeGroup(ageGroup2);
                } else if (age >= 21 && age < 25) {
                    ticket.setAgeGroup(ageGroup3);
                } else {
                    ticket.setAgeGroup(ageGroup4);
                }
                ticket.setAge(age);
                ticket.setSalesPoint(entranceSalesPoints.get(RANDOM.nextInt(entranceSalesPoints.size())));
                int ticketType = RANDOM.nextInt(10);
                if (ticketType < 7) {
                    ticket.setTicketType(dayTicketTypes.get(RANDOM.nextInt(dayTicketTypes.size())));
                } else if (ticketType < 9) {
                    ticket.setTicketType(combiTicket);
                } else {
                    ticket.setTicketType(vipTicketTypes.get(RANDOM.nextInt(vipTicketTypes.size())));
                }
                TrackingBracelet bracelet = new TrackingBracelet();
                ticket.setTrackingBracelet(bracelet);
                tickets.add(ticket);
            }
            List<Ticket> allTickets = new ArrayList<Ticket>(journalistTickets);
            allTickets.addAll(tickets);

            festival.setTickets(allTickets);

        /*
        Tracking
         */
            for (Ticket ticket : allTickets) {
                for (int i = 0; i < 10; i++) {
                    Date time = DateHelper.createTime(RANDOM.nextInt(8) + 10, RANDOM.nextInt(60));
                    Tracking tracking = new Tracking(time, RANDOM.nextBoolean());
                    tracking.setTrackingBracelet(ticket.getTrackingBracelet());
                    tracking.setFacility(allFacilities.get(RANDOM.nextInt(allFacilities.size())));
                    festival.addTracking(tracking);
                }
            }

            session.saveOrUpdate(festival);
            tx.commit();
        }
        time = System.currentTimeMillis() - time;
        System.out.println(time);
    }
}

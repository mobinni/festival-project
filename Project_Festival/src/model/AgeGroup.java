package model;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: M
 * Date: 1/11/13
 * Time: 17:22
 * To change this template use File | Settings | File Templates.
 */

@Entity
@Table(name = "t_ageGroup")
public class AgeGroup {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer ageGroupId;
    private Integer startAge;
    private Integer endAge;
    private String name;

    @OneToMany(mappedBy = "ageGroup")
    @Cascade(CascadeType.SAVE_UPDATE)
    private List<Ticket> tickets = new ArrayList<Ticket>();

    @ManyToMany(mappedBy = "ageGroups")
    @Cascade(CascadeType.SAVE_UPDATE)
    private List<Festival> festivals = new ArrayList<Festival>();

    public AgeGroup() {
    }

    // [startAge, endAge[
    public AgeGroup(Integer startAge, Integer endAge) {
        this.startAge = startAge;
        this.endAge = endAge;
        this.name = String.format("[%d-%d]", startAge, endAge);
    }

    public List<Ticket> getTickets() {
        return tickets;
    }

    public void setTickets(List<Ticket> tickets) {
        for (Ticket ticket : tickets)
            addTicket(ticket);
    }

    public Integer getEndAge() {
        return endAge;
    }

    public Integer getStartAge() {
        return startAge;
    }

    public void setStartAge(Integer startAge) {
        this.startAge = startAge;
    }

    public void setEndAge(Integer endAge) {
        this.endAge = endAge;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void addTicket(Ticket ticket) {
        tickets.add(ticket);
    }

    public void addFestival(Festival festival) {
        festivals.add(festival);
    }
}

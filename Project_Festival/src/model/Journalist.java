package model;

import org.hibernate.annotations.*;
import org.hibernate.annotations.CascadeType;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.security.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: M
 * Date: 18/10/13
 * Time: 10:01
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name="t_journalist")
public class Journalist {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer journalistId;
    private String firstName;
    private String lastName;
    private String company;

    @ManyToMany
    @JoinTable(
            name = "t_journalist_artistinfo",
            joinColumns = {@JoinColumn(name = "journalistId")},
            inverseJoinColumns = {@JoinColumn(name = "artistInfoId")}
    )
    @Cascade(CascadeType.SAVE_UPDATE)
    private List<ArtistInfo> artistInfos = new ArrayList<ArtistInfo>();

    @OneToMany
    @JoinTable(
            name = "t_journalist_ticket",
            joinColumns = {@JoinColumn(name = "journalistId")},
            inverseJoinColumns = {@JoinColumn(name = "ticketId")}
    )
    @Cascade(CascadeType.SAVE_UPDATE)
    private List<Ticket> tickets = new ArrayList<Ticket>();

    @ManyToMany(mappedBy = "journalists")
    @Cascade(CascadeType.SAVE_UPDATE)
    private List<Festival> festivals = new ArrayList<Festival>();

    public Journalist() {
    }

    public Journalist(String firstName, String lastName, String company) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.company = company;
    }

    public Integer getJournalistId() {
        return journalistId;
    }

    private void setJournalistId(Integer journalistId) {
        this.journalistId = journalistId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public List<ArtistInfo> getArtistInfos() {
        return artistInfos;
    }

    public void setArtistInfos(List<ArtistInfo> artistInfos) {
        for(ArtistInfo artistInfo : artistInfos){
            addArtistInfo(artistInfo);
        }
    }

    public void addArtistInfo(ArtistInfo artistInfo){
        artistInfos.add(artistInfo);
        artistInfo.addJournalist(this);
    }

    public List<Ticket> getTickets() {
        return tickets;
    }

    public void setTickets(List<Ticket> tickets) {
        for(Ticket ticket : tickets){
            addTicket(ticket);
        }
    }

    public void addTicket(Ticket ticket){
        tickets.add(ticket);
    }

    public List<Festival> getFestivals() {
        return festivals;
    }

    public void setFestivals(List<Festival> festivals) {
        this.festivals = festivals;
    }

    /*
    This method is called automatically when adding Journalist to Festival
     */
    public void addFestival(Festival festival){
        festivals.add(festival);
    }
}

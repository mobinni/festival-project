package model;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * Created with IntelliJ IDEA.
 * User: M
 * Date: 18/10/13
 * Time: 10:01
 * To change this template use File | Settings | File Templates.
 */
@Entity
@DiscriminatorValue("WebSale")
public class WebSale extends SalesPoint {

    @Column(name = "website")
    private String website;

    public WebSale() {
    }

    public WebSale(String name, String website) {
        super(name);
        this.website = website;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getName(){
        return super.getName();
    }
}

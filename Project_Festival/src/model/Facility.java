package model;

import org.hibernate.annotations.*;
import org.hibernate.annotations.CascadeType;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: M
 * Date: 18/10/13
 * Time: 10:01
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "t_facility")
public class Facility {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer facilityId;
    private String name;
    private String color;

    @ManyToOne
    @JoinColumn(name = "facilityTypeId")
    @Cascade(CascadeType.SAVE_UPDATE)
    private FacilityType facilityType;

    @OneToOne
    @JoinColumn(name = "subFacilityId", nullable = true)
    @Cascade(CascadeType.SAVE_UPDATE)
    private Facility facility;

    @ManyToOne
    @JoinColumn(name = "festivalId")
    @Cascade(CascadeType.SAVE_UPDATE)
    private Festival festival;

    @OneToMany(mappedBy = "facility")
    @Cascade(CascadeType.SAVE_UPDATE)
    private List<Performance> performances = new ArrayList<Performance>();

    @OneToMany(mappedBy = "facility")
    @Cascade(CascadeType.SAVE_UPDATE)
    private List<Artist> artists = new ArrayList<Artist>();

    @OneToMany(mappedBy = "facility")
    @Cascade(CascadeType.SAVE_UPDATE)
    private List<Tracking> trackings = new ArrayList<Tracking>();

    @ElementCollection(targetClass = Long.class)
    @CollectionTable(name = "t_facility_visittimes")
    private List<Long> visitTimes = new ArrayList<Long>();

    public Facility() {
    }

    public Facility(String name, String color) {
        this.name = name;
        this.color = color;
    }

    public Integer getFacilityId() {
        return facilityId;
    }

    private void setFacilityId(Integer facilityId) {
        this.facilityId = facilityId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public FacilityType getFacilityType() {
        return facilityType;
    }

    public void setFacilityType(FacilityType facilityType) {
        this.facilityType = facilityType;
        facilityType.addFacility(this);
    }

    public Facility getFacility() {
        return facility;
    }

    public void setFacility(Facility facility) {
        this.facility = facility;
    }

    public List<Performance> getPerformances() {
        return performances;
    }

    public void setPerformances(List<Performance> performances) {
        this.performances = performances;
    }

    /*
    This method is called automatically when adding Facility to Performance
     */
    public void addPerformance(Performance performance){
        performances.add(performance);
    }

    public Festival getFestival() {
        return festival;
    }

    /*
    This method is called automatically when adding Facility to Festival
     */
    public void setFestival(Festival festival) {
        this.festival = festival;
    }

    public List<Artist> getArtists() {
        return artists;
    }

    public void setArtists(List<Artist> artists) {
        this.artists = artists;
    }

    /*
    This method is called automatically when adding a facility to an artist
     */
    public void addArtist(Artist artist){
        artists.add(artist);
    }

    public List<Tracking> getTrackings() {
        return trackings;
    }

    public void setTrackings(List<Tracking> trackings) {
        this.trackings = trackings;
    }

    /*
    This method is called automatically when setting the Facility of a Tracking
     */
    public void addTracking(Tracking tracking){
        trackings.add(tracking);
    }

    public List<Long> getVisitTimes() {
        return visitTimes;
    }

    public void setVisitTimes(List<Long> visitTimes) {
        this.visitTimes = visitTimes;
    }
}

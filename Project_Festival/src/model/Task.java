package model;

import org.hibernate.annotations.*;
import org.hibernate.annotations.CascadeType;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: M
 * Date: 18/10/13
 * Time: 10:01
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "t_task")
public class Task {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer taskId;
    @Type(type = "text")
    private String description;

    @ManyToOne
    @JoinColumn(name = "festivalDayId")
    @Cascade(CascadeType.SAVE_UPDATE)
    private FestivalDay festivalDay;

    @ManyToOne
    @JoinColumn(name = "festivalId")
    @Cascade(CascadeType.SAVE_UPDATE)
    private Festival festival;

    @ManyToMany(mappedBy = "tasks")
    @Cascade(CascadeType.SAVE_UPDATE)
    private List<Employee> employees = new ArrayList<Employee>();

    public Task() {
    }

    public Task(String description) {
        this.description = description;
    }

    public FestivalDay getFestivalDay() {
        return festivalDay;
    }

    public void setFestivalDay(FestivalDay festivalDay) {
        this.festivalDay = festivalDay;
        festivalDay.addTask(this);
    }

    public List<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(List<Employee> employees) {
        for(Employee employee : employees){
            addEmployee(employee);
        }
    }

    public void addEmployee(Employee employee) {
        employees.add(employee);
        employee.addTask(this);
    }

    public Festival getFestival() {
        return festival;
    }

    public void setFestival(Festival festival) {
        this.festival = festival;
    }

    public Integer getTaskId() {
        return taskId;
    }

    private void setTaskId(Integer taskId) {
        this.taskId = taskId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}

package model;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import javax.persistence.*;
import java.util.UUID;

/**
 * Created with IntelliJ IDEA.
 * User: M
 * Date: 18/10/13
 * Time: 10:01
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "t_ticket")
public class Ticket {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer ticketId;
    @Column(unique = true)
    private String barcode;
    private Integer age;

    @ManyToOne
    @Cascade(CascadeType.SAVE_UPDATE)
    @JoinColumn(name = "ticketTypeId")
    private TicketType ticketType;

    @ManyToOne
    @Cascade(CascadeType.SAVE_UPDATE)
    @JoinColumn(name = "salesPointId")
    private SalesPoint salesPoint;

    @OneToOne
    @JoinColumn(name = "trackingBraceletId")
    @Cascade(CascadeType.SAVE_UPDATE)
    private TrackingBracelet trackingBracelet;

    @ManyToOne
    @JoinColumn(name = "ageGroupId")
    @Cascade(CascadeType.SAVE_UPDATE)
    private AgeGroup ageGroup;

    private String ageGroupName;

    @ManyToOne
    @Cascade(CascadeType.SAVE_UPDATE)
    @JoinColumn(name = "festivalId")
    private Festival festival;

    public Ticket() {
        barcode = UUID.randomUUID().toString();
    }

    public String getAgeGroupName() {
        return ageGroupName;
    }

    public void setAgeGroupName(String ageGroupName) {
        this.ageGroupName = ageGroupName;
    }

    public AgeGroup getAgeGroup() {
        return ageGroup;
    }

    public void setAgeGroup(AgeGroup ageGroup) {
        ageGroup.addTicket(this);
        this.ageGroupName = ageGroup.getName();
        this.ageGroup = ageGroup;
    }

    public TrackingBracelet getTrackingBracelet() {
        return trackingBracelet;
    }

    public void setTrackingBracelet(TrackingBracelet trackingBracelet) {
        this.trackingBracelet = trackingBracelet;
        trackingBracelet.setTicket(this);
    }

    public SalesPoint getSalesPoint() {
        return salesPoint;
    }

    public void setSalesPoint(SalesPoint salesPoint) {
        this.salesPoint = salesPoint;
        salesPoint.addTicket(this);
    }

    public void setTicketType(TicketType ticketType) {
        this.ticketType = ticketType;
        ticketType.addTicket(this);
    }

    public TicketType getTicketType() {
        return ticketType;
    }

    public Integer getTicketId() {
        return ticketId;
    }

    private void setTicketId(Integer ticketId) {
        this.ticketId = ticketId;
    }

    public String getBarcode() {
        return barcode;
    }

    private void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public Festival getFestival() {
        return festival;
    }

    /*
    This method is called automatically when adding Ticket to Festival
     */
    public void setFestival(Festival festival) {
        this.festival = festival;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }
}

package model;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Thomas
 * Date: 24/10/13
 * Time: 18:35
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "t_facilitytype")
public class FacilityType {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer facilityTypeId;
    private String type;

    @OneToMany(mappedBy = "facilityType")
    @Cascade(CascadeType.SAVE_UPDATE)
    private List<Facility> facilities = new ArrayList<Facility>();

    @ManyToMany(mappedBy = "facilityTypes")
    @Cascade(CascadeType.SAVE_UPDATE)
    private List<TicketType> ticketTypes = new ArrayList<TicketType>();

    @ManyToOne
    @JoinColumn(name = "festivalId")
    @Cascade(CascadeType.SAVE_UPDATE)
    private Festival festival;

    public FacilityType() {
    }

    public FacilityType(String type) {
        this.type = type;
    }

    public Integer getFacilityTypeId() {
        return facilityTypeId;
    }

    private void setFacilityTypeId(Integer facilityTypeId) {
        this.facilityTypeId = facilityTypeId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<Facility> getFacilities() {
        return facilities;
    }

    public void setFacilities(List<Facility> facilities) {
        this.facilities = facilities;
    }

    /*
    This method is called automatically when setting the facilityType of a facility
     */
    public void addFacility(Facility facility) {
        facilities.add(facility);
    }

    public List<TicketType> getTicketTypes() {
        return ticketTypes;
    }

    public void setTicketTypes(List<TicketType> ticketTypes) {
        this.ticketTypes = ticketTypes;
    }

    /*
    This method is called automatically when adding a FacilityType to a TicketType
     */
    public void addTicketType(TicketType ticketType){
        ticketTypes.add(ticketType);
    }

    public Festival getFestival() {
        return festival;
    }

    /*
    This method is called automatically when adding FacilityType to Festival
     */
    public void setFestival(Festival festival) {
        this.festival = festival;
    }
}

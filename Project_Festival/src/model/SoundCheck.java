package model;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import javax.persistence.*;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: M
 * Date: 18/10/13
 * Time: 10:01
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "t_soundcheck")
public class SoundCheck {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer soundCheckId;
    private Date startTime;
    private Date endTime;

    @OneToOne(mappedBy = "soundCheck")
    @Cascade(CascadeType.SAVE_UPDATE)
    private Performance performance;

    public SoundCheck() {
    }


    public SoundCheck(Date startTime, Date endTime) {
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public Integer getSoundCheckId() {
        return soundCheckId;
    }

    private void setSoundCheckId(Integer soundCheckId) {
        this.soundCheckId = soundCheckId;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Performance getPerformance() {
        return performance;
    }

    /*
    This method is called automatically when setting the SoundCheck in a Performance
     */
    public void setPerformance(Performance performance) {
        this.performance = performance;
    }
}

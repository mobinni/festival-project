package model;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: M
 * Date: 18/10/13
 * Time: 10:01
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "t_festivalday")
public class FestivalDay implements Comparable<FestivalDay> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer festivalDayId;

    @Type(type = "date")
    private Date date;

    @OneToMany(mappedBy = "festivalDay")
    @Cascade(CascadeType.SAVE_UPDATE)
    private List<Performance> performances = new ArrayList<Performance>();

    @OneToMany(mappedBy = "festivalDay")
    @Cascade(CascadeType.SAVE_UPDATE)
    private List<Task> tasks = new ArrayList<Task>();

    @ManyToOne
    @JoinColumn(name = "festivalId")
    @Cascade(CascadeType.SAVE_UPDATE)
    private Festival festival;

    @ManyToMany(mappedBy = "festivalDays")
    @Cascade(CascadeType.SAVE_UPDATE)
    private List<TicketType> ticketTypes = new ArrayList<TicketType>();

    public FestivalDay() {
    }

    public FestivalDay(Date date) {
        this.date = date;
    }

    public Integer getFestivalDayId() {
        return festivalDayId;
    }

    private void setFestivalDayId(Integer festivalDayId) {
        this.festivalDayId = festivalDayId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public List<Performance> getPerformances() {
        return performances;
    }

    public void setPerformances(List<Performance> performances) {
        this.performances = performances;
    }

    public List<Task> getTasks() {
        return tasks;
    }

    public void setTasks(List<Task> tasks) {
        this.tasks = tasks;
    }

    /*
    This method is called automatically when adding FestivalDay to Task
     */
    public void addTask(Task task) {
        tasks.add(task);
    }

    /*
        This method is called automatically when setting FestivalDay to Performance
         */
    public void addPerformance(Performance performance) {
        performances.add(performance);
    }

    public Festival getFestival() {
        return festival;
    }

    /*
    This method is called automatically when adding FestivalDay to Festival
     */
    public void setFestival(Festival festival) {
        this.festival = festival;
    }

    public List<TicketType> getTicketTypes() {
        return ticketTypes;
    }

    public void setTicketTypes(List<TicketType> ticketTypes) {
        this.ticketTypes = ticketTypes;
    }

    /*
    This method is called automatically when adding FestivalDay to TicketType
     */
    public void addTicketType(TicketType ticketType) {
        ticketTypes.add(ticketType);
    }

    @Override
    public int compareTo(FestivalDay o) {
        return this.getDate().compareTo(o.getDate());
    }
}

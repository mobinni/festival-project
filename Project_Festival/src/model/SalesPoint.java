package model;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: M
 * Date: 18/10/13
 * Time: 10:01
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "t_salespoint")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "SalesPointType",
        discriminatorType = DiscriminatorType.STRING)
@DiscriminatorValue("SalesPoint")
public class SalesPoint {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer salesPointId;
    private String name;

    @OneToMany(mappedBy = "salesPoint")
    @Cascade(CascadeType.SAVE_UPDATE)
    private List<Ticket> tickets = new ArrayList<Ticket>();

    @ManyToMany(mappedBy = "salesPoints")
    @Cascade(CascadeType.SAVE_UPDATE)
    private List<Festival> festivals = new ArrayList<Festival>();

    public SalesPoint() {
    }

    public SalesPoint(String name) {
        this.name = name;
    }

    public List<Festival> getFestivals() {
        return festivals;
    }

    public void setFestivals(List<Festival> festivals) {
        for (Festival festival : festivals) {
            addFestival(festival);
        }
    }

    /*
        This method is called automatically when adding SalesPoint to Festival
         */
    public void addFestival(Festival festival) {
        festivals.add(festival);
    }

    public Integer getSalesPointId() {
        return salesPointId;
    }

    private void setSalesPointId(Integer salesPointId) {
        this.salesPointId = salesPointId;
    }

    public List<Ticket> getTickets() {
        return tickets;
    }

    public void setTickets(List<Ticket> tickets) {
        for (Ticket ticket : tickets) {
            addTicket(ticket);
        }
    }

    /*
    This method is called automatically when adding SalesPoint to Ticket
     */
    public void addTicket(Ticket ticket) {
        tickets.add(ticket);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

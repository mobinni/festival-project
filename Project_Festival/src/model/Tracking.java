package model;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: Thomas
 * Date: 24/10/13
 * Time: 20:31
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "t_tracking")
public class Tracking {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer trackingId;

    private Date time;

    private boolean type;

    @ManyToOne
    @JoinColumn(name = "trackingBraceletId")
    @Cascade(CascadeType.SAVE_UPDATE)
    private TrackingBracelet trackingBracelet;

    @ManyToOne
    @JoinColumn(name = "facilityId")
    @Cascade(CascadeType.SAVE_UPDATE)
    private Facility facility;

    @ManyToOne
    @JoinColumn(name = "festivalId")
    @Cascade(CascadeType.SAVE_UPDATE)
    private Festival festival;

    public Tracking() {
    }

    public Tracking(Date time, boolean type) {
        this.time = time;
        this.type = type;
    }

    public Integer getTrackingId() {
        return trackingId;
    }

    private void setTrackingId(Integer trackingId) {
        this.trackingId = trackingId;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public Facility getFacility() {
        return facility;
    }

    public void setFacility(Facility facility) {
        this.facility = facility;
        facility.addTracking(this);
    }

    public boolean isType() {
        return type;
    }

    public void setType(boolean type) {
        this.type = type;
    }

    public TrackingBracelet getTrackingBracelet() {
        return trackingBracelet;
    }

    public void setTrackingBracelet(TrackingBracelet trackingBracelet) {
        this.trackingBracelet = trackingBracelet;
        trackingBracelet.addTracking(this);
    }

    public Festival getFestival() {
        return festival;
    }

    /*
    This method is called automatically when adding Tracking to Festival
     */
    public void setFestival(Festival festival) {
        this.festival = festival;
    }
}

package model;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: M
 * Date: 18/10/13
 * Time: 10:01
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "t_performance")
public class Performance {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer performanceId;
    private Date startTime;
    private Date endTime;
    private String name;

    @ManyToOne
    @JoinColumn(name = "festivalId")
    @Cascade(CascadeType.SAVE_UPDATE)
    private Festival festival;

    @ManyToOne
    @JoinColumn(name = "facilityId")
    @Cascade(CascadeType.SAVE_UPDATE)
    private Facility facility;

    @ManyToMany
    @JoinTable(
            name = "t_performance_need",
            joinColumns = {@JoinColumn(name = "performanceId")},
            inverseJoinColumns = {@JoinColumn(name = "needId")}
    )
    @Cascade(CascadeType.SAVE_UPDATE)
    private List<Need> needs = new ArrayList<Need>();

    @ManyToOne
    @JoinColumn(name = "artistId")
    @Cascade(CascadeType.SAVE_UPDATE)
    private Artist artist;

    @OneToOne
    @JoinColumn(name = "soundCheckId")
    @Cascade(CascadeType.SAVE_UPDATE)
    private SoundCheck soundCheck;

    @ManyToOne
    @JoinColumn(name = "festivalDayId")
    @Cascade(CascadeType.SAVE_UPDATE)
    private FestivalDay festivalDay;

    public Performance() {
    }

    public Performance(String name, Date startTime, Date endTime) {
        this.name = name;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Integer getPerformanceId() {
        return performanceId;
    }

    private void setPerformanceId(Integer performanceId) {
        this.performanceId = performanceId;
    }

    public Festival getFestival() {
        return festival;
    }

    /*
    This method is automatically called when adding festival to performance
     */
    public void setFestival(Festival festival) {
        this.festival = festival;
    }

    public Facility getFacility() {
        return facility;
    }

    public void setFacility(Facility facility) {
        this.facility = facility;
        facility.addPerformance(this);
    }

    public List<Need> getNeeds() {
        return needs;
    }

    public void setNeeds(List<Need> needs) {
        for (Need need : needs) {
            addNeed(need);
        }
    }

    public void addNeed(Need need) {
        needs.add(need);
        need.addPerformance(this);
    }

    public Artist getArtist() {
        return artist;
    }

    /*
    This method is called automatically when a Performance is added to an Artist
     */
    public void setArtist(Artist artist) {
        this.artist = artist;
    }

    public SoundCheck getSoundCheck() {
        return soundCheck;
    }

    public void setSoundCheck(SoundCheck soundCheck) {
        this.soundCheck = soundCheck;
        soundCheck.setPerformance(this);
    }

    public FestivalDay getFestivalDay() {
        return festivalDay;
    }

    public void setFestivalDay(FestivalDay festivalDay) {
        this.festivalDay = festivalDay;
        festivalDay.addPerformance(this);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Performance that = (Performance) o;

        if (artist != null ? !artist.equals(that.artist) : that.artist != null) return false;
        if (endTime != null ? !endTime.equals(that.endTime) : that.endTime != null) return false;
        if (facility != null ? !facility.equals(that.facility) : that.facility != null) return false;
        if (festival != null ? !festival.equals(that.festival) : that.festival != null) return false;
        if (festivalDay != null ? !festivalDay.equals(that.festivalDay) : that.festivalDay != null) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (needs != null ? !needs.equals(that.needs) : that.needs != null) return false;
        if (performanceId != null ? !performanceId.equals(that.performanceId) : that.performanceId != null)
            return false;
        if (soundCheck != null ? !soundCheck.equals(that.soundCheck) : that.soundCheck != null) return false;
        if (startTime != null ? !startTime.equals(that.startTime) : that.startTime != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = performanceId != null ? performanceId.hashCode() : 0;
        result = 31 * result + (startTime != null ? startTime.hashCode() : 0);
        result = 31 * result + (endTime != null ? endTime.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (festival != null ? festival.hashCode() : 0);
        result = 31 * result + (facility != null ? facility.hashCode() : 0);
        result = 31 * result + (needs != null ? needs.hashCode() : 0);
        result = 31 * result + (artist != null ? artist.hashCode() : 0);
        result = 31 * result + (soundCheck != null ? soundCheck.hashCode() : 0);
        result = 31 * result + (festivalDay != null ? festivalDay.hashCode() : 0);
        return result;
    }
}

package model;

import org.hibernate.annotations.*;
import org.hibernate.annotations.CascadeType;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: M
 * Date: 18/10/13
 * Time: 10:01
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "t_location")
public class Location {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer locationId;

    private Integer maxVisitors;
    private String address;

    @OneToMany(mappedBy = "location")
    @Cascade(CascadeType.SAVE_UPDATE)
    private List<Festival> festivals = new ArrayList<Festival>();

    public Location() {
    }


    public Location(Integer maxVisitors, String address) {
        this.maxVisitors = maxVisitors;
        this.address = address;
    }

    public Integer getLocationId() {
        return locationId;
    }

    private void setLocationId(Integer locationId) {
        this.locationId = locationId;
    }

    public Integer getMaxVisitors() {
        return maxVisitors;
    }

    public void setMaxVisitors(Integer maxVisitors) {
        this.maxVisitors = maxVisitors;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public List<Festival> getFestivals() {
        return festivals;
    }

    public void setFestivals(List<Festival> festivals) {
        this.festivals = festivals;
    }

    /*
    This method is automatically called when adding location to festival
     */
    public void addFestival(Festival festival){
        festivals.add(festival);
    }
}

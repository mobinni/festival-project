package model;

import org.hibernate.annotations.*;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.Parameter;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created with IntelliJ IDEA.
 * User: M
 * Date: 18/10/13
 * Time: 10:01
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "t_trackingbracelet")
public class TrackingBracelet {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer braceletId;

    @Column(unique = true)
    private String serialNo;

    @OneToMany(mappedBy = "trackingBracelet")
    @Cascade(CascadeType.SAVE_UPDATE)
    private List<Tracking> trackings = new ArrayList<Tracking>();

    @OneToOne(mappedBy = "trackingBracelet")
    @Cascade(CascadeType.SAVE_UPDATE)
    private Ticket ticket;

    public TrackingBracelet() {
        this.serialNo = UUID.randomUUID().toString();
    }

    public Integer getBraceletId() {
        return braceletId;
    }

    private void setBraceletId(Integer braceletId) {
        this.braceletId = braceletId;
    }

    public String getSerialNo() {
        return serialNo;
    }

    public void setSerialNo(String serialNo) {
        this.serialNo = serialNo;
    }

    public List<Tracking> getTrackings() {
        return trackings;
    }

    public void setTrackings(List<Tracking> trackings) {
        for(Tracking tracking : trackings){
            addTracking(tracking);
        }
    }

    /*
    This method is called automatically when adding TrackingBracelet to Tracking
     */
    public void addTracking(Tracking tracking){
        trackings.add(tracking);
    }

    public Ticket getTicket() {
        return ticket;
    }

    /*
    This method is called automatically when adding TrackingBracelet to Ticket
     */
    public void setTicket(Ticket ticket) {
        this.ticket = ticket;
    }
}

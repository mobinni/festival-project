package model;

import org.hibernate.annotations.*;
import org.hibernate.annotations.CascadeType;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created with IntelliJ IDEA.
 * User: M
 * Date: 18/10/13
 * Time: 10:01
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name="t_song")
public class Song {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer songId;
    private String title;
    private Integer duration;

    @ManyToOne
    @JoinColumn(name = "artistId")
    @Cascade(CascadeType.SAVE_UPDATE)
    private Artist artist;

    public Song() {
    }

    public Song(String title, Integer duration) {
        this.title = title;
        this.duration = duration;
    }

    public Integer getSongId() {
        return songId;
    }

    private void setSongId(Integer songId) {
        this.songId = songId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public Artist getArtist() {
        return artist;
    }

    /*
    This method is automatically called when adding a Song to an Artist
     */
    public void setArtist(Artist artist) {
        this.artist = artist;
    }
}

package model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * Created with IntelliJ IDEA.
 * User: M
 * Date: 18/10/13
 * Time: 10:01
 * To change this template use File | Settings | File Templates.
 */
@Entity
@DiscriminatorValue("EntranceSale")
public class EntranceSale extends SalesPoint {

    public EntranceSale() {
    }

    public EntranceSale(String name) {
        super(name);
    }

    public String getName(){
        return super.getName();
    }
}

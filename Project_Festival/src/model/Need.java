package model;

import org.hibernate.annotations.*;
import org.hibernate.annotations.CascadeType;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: M
 * Date: 18/10/13
 * Time: 10:01
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name="t_need")
public class Need {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer needId;
    private String description;
    private Integer amount;

    @ManyToMany(mappedBy = "needs")
    @Cascade(CascadeType.SAVE_UPDATE)
    private List<Performance> performances = new ArrayList<Performance>();

    public Need() {
    }

    public Need(String description, Integer amount) {
        this.description = description;
        this.amount = amount;
    }

    public Integer getNeedId() {
        return needId;
    }

    private void setNeedId(Integer needId) {
        this.needId = needId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public List<Performance> getPerformances() {
        return performances;
    }

    public void setPerformances(List<Performance> performances) {
        this.performances = performances;
    }

    /*
    This method is called automatically when adding Need to Performance
     */
    public void addPerformance(Performance performance){
        performances.add(performance);
    }
}

package model;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * Created with IntelliJ IDEA.
 * User: M
 * Date: 18/10/13
 * Time: 10:01
 * To change this template use File | Settings | File Templates.
 */
@Entity
@DiscriminatorValue("StoreSale")
public class StoreSale extends SalesPoint {


    @Column(name = "address")
    private String address;

    public StoreSale() {
    }

    public StoreSale(String name, String address) {
        super(name);
        this.address = address;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getName(){
        return super.getName();
    }
}

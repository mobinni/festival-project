package model;

import org.hibernate.annotations.*;
import org.hibernate.annotations.CascadeType;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: M
 * Date: 18/10/13
 * Time: 10:01
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "t_artist")
public class Artist {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer artistId;
    private String name;
    private String managerName;

    @ManyToOne
    @JoinColumn(name = "facilityId")
    @Cascade(CascadeType.SAVE_UPDATE)
    private Facility facility;

    @OneToMany(mappedBy = "artist")
    @Cascade(CascadeType.SAVE_UPDATE)
    private List<Performance> performances = new ArrayList<Performance>();

    @OneToOne
    @JoinColumn(name = "artistInfoId")
    @Cascade(CascadeType.SAVE_UPDATE)
    private ArtistInfo artistInfo;

    @OneToMany(mappedBy = "artist")
    @Cascade(CascadeType.SAVE_UPDATE)
    private List<Song> songs = new ArrayList<Song>();

    public Artist() {
    }

    public Artist(String name, String managerName) {
        this.name = name;
        this.managerName = managerName;
    }

    public Integer getArtistId() {
        return artistId;
    }

    private void setArtistId(Integer artistId) {
        this.artistId = artistId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getManagerName() {
        return managerName;
    }

    public void setManagerName(String managerName) {
        this.managerName = managerName;
    }

    public Facility getFacility() {
        return facility;
    }

    public void setFacility(Facility facility) {
        this.facility = facility;
        facility.addArtist(this);
    }

    public List<Performance> getPerformances() {
        return performances;
    }

    public void setPerformances(List<Performance> performances) {
        for(Performance performance : performances){
            addPerformance(performance);
        }
    }

    public void addPerformance(Performance performance){
        performances.add(performance);
        performance.setArtist(this);
    }

    public ArtistInfo getArtistInfo() {
        return artistInfo;
    }

    public void setArtistInfo(ArtistInfo artistInfo) {
        this.artistInfo = artistInfo;
        artistInfo.setArtist(this);
    }

    public List<Song> getSongs() {
        return songs;
    }

    public void setSongs(List<Song> songs) {
        this.songs = songs;
    }

    public void addSong(Song song){
        songs.add(song);
        song.setArtist(this);
    }
}

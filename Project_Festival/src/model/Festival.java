package model;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: M
 * Date: 18/10/13
 * Time: 10:01
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "t_festival")
public class Festival {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer festivalId;

    private String name;
    /*
    Voor deel 4: optimalisatie een berekend veld start en einddatum toevoegen
     */

    private Date startTime;

    private Date endTime;

    private Double totalSold = 0.0;

    @ManyToOne
    @JoinColumn(name = "locationId")
    @Cascade(CascadeType.SAVE_UPDATE)
    private Location location;

    @OneToMany(mappedBy = "festival")
    @Cascade(CascadeType.SAVE_UPDATE)
    private List<Facility> facilities = new ArrayList<Facility>();

    @OneToMany(mappedBy = "festival")
    @Cascade(CascadeType.SAVE_UPDATE)
    private List<Performance> performances = new ArrayList<Performance>();

    @ManyToMany
    @JoinTable(
            name = "t_festival_salespoints",
            joinColumns = {@JoinColumn(name = "festivalId")},
            inverseJoinColumns = {@JoinColumn(name = "salesPointId")}
    )
    @Cascade({CascadeType.SAVE_UPDATE})
    private List<SalesPoint> salesPoints = new ArrayList<SalesPoint>();

    @OneToMany(mappedBy = "festival")
    @Cascade({CascadeType.SAVE_UPDATE})
    private List<TicketType> ticketTypes = new ArrayList<TicketType>();

    @OneToMany(mappedBy = "festival")
    @Cascade({CascadeType.SAVE_UPDATE})
    private List<Task> tasks = new ArrayList<Task>();

    @OneToMany(mappedBy = "festival")
    @Cascade({CascadeType.SAVE_UPDATE})
    private List<Employee> employees = new ArrayList<Employee>();

    @OneToMany(mappedBy = "festival")
    @Cascade(CascadeType.SAVE_UPDATE)
    private List<ArtistInfo> artistInfos = new ArrayList<ArtistInfo>();

    @OneToMany(mappedBy = "festival")
    @Cascade(CascadeType.SAVE_UPDATE)
    private List<FacilityType> facilityTypes = new ArrayList<FacilityType>();

    @ManyToMany
    @JoinTable(
            name = "t_festival_journalist",
            joinColumns = {@JoinColumn(name = "festivalId")},
            inverseJoinColumns = {@JoinColumn(name = "journalistId")}
    )
    @Cascade(CascadeType.SAVE_UPDATE)
    private List<Journalist> journalists = new ArrayList<Journalist>();

    @OneToMany(mappedBy = "festival")
    @Cascade(CascadeType.SAVE_UPDATE)
    private List<FestivalDay> festivalDays = new ArrayList<FestivalDay>();

    @OneToMany(mappedBy = "festival")
    @Cascade(CascadeType.SAVE_UPDATE)
    private List<Tracking> trackings = new ArrayList<Tracking>();

    @OneToMany(mappedBy = "festival")
    @Cascade(CascadeType.SAVE_UPDATE)
    private List<Ticket> tickets = new ArrayList<Ticket>();

    @ManyToMany
    @JoinTable(
            name = "t_festival_agegroups",
            joinColumns = {@JoinColumn(name = "festivalId")},
            inverseJoinColumns = {@JoinColumn(name = "ageGroupId")}
    )
    @Cascade({CascadeType.SAVE_UPDATE})
    @OrderBy("startAge")
    private List<AgeGroup> ageGroups = new ArrayList<AgeGroup>();

    public Festival() {
    }

    public Festival(String name, Date startTime, Date endTime) {
        this.name = name;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public Integer getFestivalId() {
        return festivalId;
    }

    private void setFestivalId(Integer festivalId) {
        this.festivalId = festivalId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
        location.addFestival(this);
    }

    public List<SalesPoint> getSalesPoints() {
        return salesPoints;
    }

    public void setSalesPoints(List<SalesPoint> salesPoints) {
        for (SalesPoint salesPoint : salesPoints) {
            addSalesPoint(salesPoint);
        }
    }

    public void addSalesPoint(SalesPoint salesPoint) {
        salesPoints.add(salesPoint);
        salesPoint.addFestival(this);
    }

    public List<TicketType> getTicketTypes() {
        return ticketTypes;
    }

    public void setTicketTypes(List<TicketType> ticketTypes) {
        for (TicketType ticketType : ticketTypes) {
            addTicketType(ticketType);
        }
    }

    public void addTicketType(TicketType ticketType) {
        ticketTypes.add(ticketType);
        ticketType.setFestival(this);
    }

    public List<Task> getTasks() {
        return tasks;
    }

    public void setTasks(List<Task> tasks) {
        for (Task task : tasks) {
            addTask(task);
        }
    }

    public void addTask(Task task) {
        tasks.add(task);
        task.setFestival(this);
    }

    public List<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(List<Employee> employees) {
        for (Employee employee : employees) {
            addEmployee(employee);
        }
    }

    public void addEmployee(Employee employee) {
        employees.add(employee);
        employee.setFestival(this);
    }

    public List<Performance> getPerformances() {
        return performances;
    }

    public void setPerformances(List<Performance> performances) {
        for (Performance performance : performances) {
            addPerformance(performance);
        }
    }

    public void addPerformance(Performance performance) {
        performances.add(performance);
        performance.setFestival(this);
    }

    public List<Facility> getFacilities() {
        return facilities;
    }

    public void setFacilities(List<Facility> facilities) {
        for (Facility facility : facilities) {
            addFacility(facility);
        }
    }

    public void addFacility(Facility facility) {
        facilities.add(facility);
        facility.setFestival(this);
    }

    public List<ArtistInfo> getArtistInfos() {
        return artistInfos;
    }

    public void setArtistInfos(List<ArtistInfo> artistInfos) {
        for (ArtistInfo artistInfo : artistInfos) {
            addArtistInfo(artistInfo);
        }
    }

    public void addArtistInfo(ArtistInfo artistInfo) {
        artistInfos.add(artistInfo);
        artistInfo.setFestival(this);
    }

    public List<FacilityType> getFacilityTypes() {
        return facilityTypes;
    }

    public void setFacilityTypes(List<FacilityType> facilityTypes) {
        for (FacilityType facilityType : facilityTypes) {
            addFacilityType(facilityType);
        }
    }

    public void addFacilityType(FacilityType facilityType) {
        facilityTypes.add(facilityType);
        facilityType.setFestival(this);
    }

    public List<Journalist> getJournalists() {
        return journalists;
    }

    public void setJournalists(List<Journalist> journalists) {
        for (Journalist journalist : journalists) {
            addJournalist(journalist);
        }
    }

    public void addJournalist(Journalist journalist) {
        journalists.add(journalist);
        journalist.addFestival(this);
    }

    public List<FestivalDay> getFestivalDays() {
        return festivalDays;
    }

    public void setFestivalDays(List<FestivalDay> festivalDays) {
        for (FestivalDay festivalDay : festivalDays) {
            addFestivalDay(festivalDay);
        }
    }

    public void addFestivalDay(FestivalDay festivalDay) {
        festivalDays.add(festivalDay);
        festivalDay.setFestival(this);
    }

    public List<Tracking> getTrackings() {
        return trackings;
    }

    public void setTrackings(List<Tracking> trackings) {
        for (Tracking tracking : trackings) {
            addTracking(tracking);
        }
    }

    public void addTracking(Tracking tracking) {
        trackings.add(tracking);
        tracking.setFestival(this);
    }

    public List<Ticket> getTickets() {
        return tickets;
    }

    public void setTickets(List<Ticket> tickets) {
        for (Ticket ticket : tickets) {
            addTicket(ticket);
        }
    }

    public void addTicket(Ticket ticket) {
        tickets.add(ticket);
        ticket.setFestival(this);
        addSale(ticket.getTicketType().getPrice());
    }

    public Double getTotalSold() {
        return totalSold;
    }

    public void setTotalSold(Double totalSold) {
        this.totalSold = totalSold;
    }

    public void addSale(Double sale) {
        totalSold += sale;
    }

    public List<AgeGroup> getAgeGroups() {
        return ageGroups;
    }

    public void setAgeGroups(List<AgeGroup> ageGroups) {
        for (AgeGroup ageGroup : ageGroups) {
            addAgeGroup(ageGroup);
        }
    }

    public void addAgeGroup(AgeGroup ageGroup) {
        ageGroups.add(ageGroup);
        ageGroup.addFestival(this);
    }
}

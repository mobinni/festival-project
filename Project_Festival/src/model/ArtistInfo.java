package model;

import org.hibernate.annotations.*;
import org.hibernate.annotations.CascadeType;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Thomas
 * Date: 24/10/13
 * Time: 22:12
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "t_artistinfo")
public class ArtistInfo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer artistInfoId;

    @Type(type = "text")
    private String biography;
    private Boolean permission; //Permission to film & take pictures

    @OneToOne(mappedBy = "artistInfo")
    @Cascade(CascadeType.SAVE_UPDATE)
    private Artist artist;

    @ManyToMany(mappedBy = "artistInfos")
    @Cascade(CascadeType.SAVE_UPDATE)
    private List<Journalist> journalists = new ArrayList<Journalist>();

    @ManyToOne
    @JoinColumn(name = "festivalId")
    @Cascade(CascadeType.SAVE_UPDATE)
    private Festival festival;

    public ArtistInfo(){
    }

    public ArtistInfo(String biography, Boolean permission) {
        this.biography = biography;
        this.permission = permission;
    }

    public Integer getArtistInfoId() {
        return artistInfoId;
    }

    private void setArtistInfoId(Integer artistInfoId) {
        this.artistInfoId = artistInfoId;
    }

    public String getBiography() {
        return biography;
    }

    public void setBiography(String biography) {
        this.biography = biography;
    }

    public Boolean isPermission() {
        return permission;
    }

    public void setPermission(Boolean permission) {
        this.permission = permission;
    }

    public Artist getArtist() {
        return artist;
    }

    /*
    This method is automatically called when setting ArtistInfo of Artist
     */
    public void setArtist(Artist artist) {
        this.artist = artist;
    }

    public List<Journalist> getJournalists() {
        return journalists;
    }

    public void setJournalists(List<Journalist> journalists) {
        this.journalists = journalists;
    }

    /*
    This method is called automatically when adding ArtistInfo to Journalist
     */
    public void addJournalist(Journalist journalist){
        journalists.add(journalist);
    }

    public Festival getFestival() {
        return festival;
    }

    /*
    This method is called automatically when adding an ArtistInfo to Festival
     */
    public void setFestival(Festival festival) {
        this.festival = festival;
    }
}

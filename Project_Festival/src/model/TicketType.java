package model;

import org.hibernate.annotations.*;
import org.hibernate.annotations.CascadeType;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: M
 * Date: 18/10/13
 * Time: 10:01
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "t_tickettype")
public class TicketType {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer ticketTypeId;
    private String name;
    private Double price;

    @OneToMany(mappedBy = "ticketType")
    @Cascade(CascadeType.SAVE_UPDATE)
    private List<Ticket> tickets = new ArrayList<Ticket>();

    @ManyToOne
    @JoinColumn(name = "festivalId")
    @Cascade(CascadeType.SAVE_UPDATE)
    private Festival festival;

    @ManyToMany
    @JoinTable(
            name = "t_tickettype_facilitytype",
            joinColumns = {@JoinColumn(name = "ticketTypeId")},
            inverseJoinColumns = {@JoinColumn(name = "facilityTypeId")}
    )
    @Cascade(CascadeType.SAVE_UPDATE)
    private List<FacilityType> facilityTypes = new ArrayList<FacilityType>();

    @ManyToMany
    @JoinTable(
            name = "t_tickettype_festivalday",
            joinColumns = {@JoinColumn(name = "ticketTypeId")},
            inverseJoinColumns = {@JoinColumn(name = "festivalDayId")}
    )
    @Cascade(CascadeType.SAVE_UPDATE)
    private List<FestivalDay> festivalDays = new ArrayList<FestivalDay>();

    public TicketType() {
    }

    public TicketType(String name, Double price) {
        this.name = name;
        this.price = price;
    }

    public Festival getFestival() {
        return festival;
    }

    /*
    This method is called automatically when adding TicketType to Festival
     */
    public void setFestival(Festival festival) {
        this.festival = festival;
    }

    /*
    This method is called automatically when adding TicketType to Ticket
     */
    public void addTicket(Ticket ticket) {
        tickets.add(ticket);
    }

    public Integer getTicketTypeId() {
        return ticketTypeId;
    }

    private void setTicketTypeId(Integer ticketTypeId) {
        this.ticketTypeId = ticketTypeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String type) {
        this.name = type;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public List<Ticket> getTickets() {
        return tickets;
    }

    public void setTickets(List<Ticket> tickets) {
        this.tickets = tickets;
    }

    public List<FacilityType> getFacilityTypes() {
        return facilityTypes;
    }

    public void setFacilityTypes(List<FacilityType> facilityTypes) {
        for(FacilityType facilityType : facilityTypes){
            addFacilityType(facilityType);
        }
    }

    public void addFacilityType(FacilityType facilityType){
        facilityTypes.add(facilityType);
        facilityType.addTicketType(this);
    }

    public List<FestivalDay> getFestivalDays() {
        return festivalDays;
    }

    public void setFestivalDays(List<FestivalDay> festivalDays) {
        for(FestivalDay festivalDay : festivalDays){
            addFestivalDay(festivalDay);
        }
    }

    public void addFestivalDay(FestivalDay festivalDay){
        festivalDays.add(festivalDay);
        festivalDay.addTicketType(this);
    }
}

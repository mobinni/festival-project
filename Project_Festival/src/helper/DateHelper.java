package helper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: Thomas
 * Date: 25/10/13
 * Time: 14:01
 * To change this template use File | Settings | File Templates.
 */
public class DateHelper {
    public static final SimpleDateFormat FORMAT_DATE = new SimpleDateFormat("dd-MM-yyyy");
    public static final SimpleDateFormat FORMAT_TIME = new SimpleDateFormat("HH:mm");
    public static final SimpleDateFormat FORMAT_TIME2 = new SimpleDateFormat("HH:mm:ss");


    public static Date createDate(int day, int month, int year) throws IllegalArgumentException, ParseException {
        if (!isValidDate(day, month, year)) throw new IllegalArgumentException("Not a valid date");

        return FORMAT_DATE.parse(String.format("%d-%d-%d", day, month, year));
    }

    public static Date createTime(int hour, int minutes) throws IllegalArgumentException, ParseException {
        if (!isValidTime(hour, minutes)) throw new IllegalArgumentException("Not a valid time");

        return FORMAT_TIME.parse(String.format("%d:%d", hour, minutes));
    }

    public static String addTime(int minutes, Date date) {
        Date time = new Date(date.getTime() + (minutes * 60 * 1000));
        return FORMAT_TIME2.format(time);
    }


    public static boolean isValidDate(int day, int month, int year) {
        if (day < 1) return false;
        if (day > daysInMonth(month, year)) return false;
        if (month < 1) return false;
        if (month > 12) return false;

        return true;
    }

    public static boolean isValidTime(int hour, int minutes) {
        return !(hour < 0 || hour > 23 || minutes < 0 || minutes > 59);
    }

    private static int daysInMonth(int month, int year) {
        switch (month) {
            case 1:
                return 31;
            case 2: {
                if (year % 4 == 0 && year % 100 != 0) {
                    return 29;
                } else {
                    return 28;
                }
            }
            case 3:
                return 31;
            case 4:
                return 30;
            case 5:
                return 31;
            case 6:
                return 30;
            case 7:
                return 31;
            case 8:
                return 31;
            case 9:
                return 30;
            case 10:
                return 31;
            case 11:
                return 30;
            case 12:
                return 31;
            default:
                return 0;
        }
    }
}

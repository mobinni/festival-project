package helper;

import javax.xml.XMLConstants;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.StringReader;

/**
 * Created with IntelliJ IDEA.
 * User: M
 * Date: 27/10/13
 * Time: 22:41
 * To change this template use File | Settings | File Templates.
 */
public class XMLHelper {

    public static boolean validateXML(String xsdLocation, String xml) {
        SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        try {
            File schemaFile = new File(xsdLocation);
            Schema schema = factory.newSchema(schemaFile);
            Validator validator = schema.newValidator();
            validator.validate(new StreamSource(new StringReader(xml)));
            return true;
        } catch (Exception e) {
            //  e.printStackTrace();
            return false;
        }
    }
}

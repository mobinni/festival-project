package tracking;

import festivalmanagement.Management;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.hibernate.*;
import persistency.HibernateUtil;

import javax.jms.*;
import javax.jms.Session;
import java.io.*;
import java.util.Scanner;

public class MessageReceiver {
    final static org.hibernate.Session hibernateSession = HibernateUtil.getSessionFactory().openSession();

    public static void main(String[] args) {
        ActiveMQConnectionFactory connectionFactory =
                new ActiveMQConnectionFactory("failover:(tcp://localhost:60601,tcp://localhost:60602)?timeout=1000");

        Scanner scanner = new Scanner(System.in);

        try {
            // Create a Connection to ActiceMQ
            Connection connection = connectionFactory.createConnection();
            connection.start();
            // Create a Session that allows you to work with activeMQ
            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            // Create the destination queue (or retrieve it, if it already exists)
            Queue destination = session.createQueue("TEST.SENDRECEIVE");

            MessageConsumer consumer = session.createConsumer(destination);
            File trackingFile = new File("..\\Logging\\tracking.log");
            File errorFile = new File("..\\Logging\\error.log");
            Writer trackingWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(trackingFile)));
            Writer errorWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(errorFile)));

            consumer.setMessageListener(new ObjectMessageListener(trackingWriter, errorWriter,hibernateSession));

            Thread.sleep(500000);
            trackingWriter.close();
            errorWriter.close();
            connection.close();
            session.close();
            //System.exit(0);
        } catch (JMSException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

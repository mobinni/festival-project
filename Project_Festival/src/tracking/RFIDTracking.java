package tracking;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

public class RFIDTracking implements Serializable {
    private Integer trackingId;
    private Date time;
    private String type;
    private Integer trackingBraceletId;
    private Integer facilityId;

    public RFIDTracking() {
    }

    public RFIDTracking(Integer trackingId, Date time, String type, Integer trackingBraceletId, Integer facilityId) {
        this.trackingId = trackingId;
        this.time = time;
        this.type = type;
        this.trackingBraceletId = trackingBraceletId;
        this.facilityId = facilityId;
    }

    public Integer getTrackingId() {
        return trackingId;
    }

    public void setTrackingId(Integer trackingId) {
        this.trackingId = trackingId;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getTrackingBraceletId() {
        return trackingBraceletId;
    }

    public void setTrackingBraceletId(int trackingBraceletId) {
        this.trackingBraceletId = trackingBraceletId;
    }

    public Integer getFacilityId() {
        return facilityId;
    }

    public void setFacilityId(int facilityId) {
        this.facilityId = facilityId;
    }
}

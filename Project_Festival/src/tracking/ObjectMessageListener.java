package tracking;

import festivalmanagement.Management;
import helper.XMLHelper;
import model.Facility;
import model.Festival;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;
import java.io.Reader;
import java.io.StringReader;
import java.io.Writer;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ObjectMessageListener implements MessageListener {
    private Writer trackingWriter;
    private Writer errorWriter;
    private MongoDbWriter mongoDbWriter;
    private Map<Integer, Integer> rfidZoneMapping;
    final Session session;

    private static final int FESTIVAL_ID = 5;

    public ObjectMessageListener(Writer trackingWriter, Writer errorWriter, Session session) {
        this.trackingWriter = trackingWriter;
        this.errorWriter = errorWriter;
        this.mongoDbWriter = new MongoDbWriter();
        this.rfidZoneMapping = new HashMap<Integer, Integer>();
        this.session = session;
        fillMap();
    }

    private void fillMap() {
        final Query festivalQuery = session.createQuery("from Festival f where f.festivalId=:festivalId");
        festivalQuery.setInteger("festivalId", FESTIVAL_ID);
        final Festival festival = (Festival) festivalQuery.uniqueResult();
        final List<Facility> facilities = festival.getFacilities();

        int[] facilityIds = new int[facilities.size()];
        //filling array
        for (int i = 0; i < facilities.size(); i++) {
            facilityIds[i] = facilities.get(i).getFacilityId();
        }

        for (int i = 1; i <= facilities.size(); i++) {
            rfidZoneMapping.put(i, facilityIds[i - 1]);
        }
    }

    @Override
    public void onMessage(Message message) {
        Transaction transaction = session.beginTransaction();
        Management management = new Management(session, transaction);
        TextMessage textMessage = (TextMessage) message;

        try {
            if (XMLHelper.validateXML(".\\src\\tracking\\TrackingBericht.xsd", textMessage.getText())) {
                final Reader reader = new StringReader(textMessage.getText());
                RFIDTracking rfidTracking = RFIDMessageSystem.getRFIDTracking(reader);
                //mapping of the facilityId
                rfidTracking.setFacilityId(rfidZoneMapping.get(rfidTracking.getFacilityId()));
                //opslaan passage

                if (management.insertVisitorPass(rfidTracking, FESTIVAL_ID) == 0) {
                    trackingWriter.write("(Id=" + rfidTracking.getTrackingId()
                            + ";time=" + rfidTracking.getTime().toString()
                            + ";braceletId=" + rfidTracking.getTrackingBraceletId()
                            + ";zoneId=" + rfidTracking.getFacilityId()
                            + ";type=" + rfidTracking.getType() + ")");
                    //insert into mongodb
                    mongoDbWriter.insertIntoDb(rfidTracking);
                } else {
                    String messageString = "Could not write to Db";
                    errorWriter.write("Error 303: Message " + messageString + " is corrupt!");
                }
                transaction.commit();
            } else {
                String messageString = textMessage.getText().split("\\(Id=")[1];
                messageString = messageString.split(";")[0];
                errorWriter.write("Error 303: Message " + messageString + " is corrupt!");
                transaction.rollback();
            }

        } catch (Exception e) {
            e.printStackTrace();
            transaction.rollback();
        }
    }
}

package tracking;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.MongoClient;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Joey
 * Date: 25/10/13
 * Time: 10:55
 * To change this template use File | Settings | File Templates.
 */
public class MongoDbWriter {
    private MongoClient mongoClient;
    private DB db;
    private DBCollection dbCollection;

    public MongoDbWriter() {
        try {
            mongoClient = new MongoClient("localhost", 27017);
            db = mongoClient.getDB("project_festival");
            dbCollection = db.getCollection("trackings");
        } catch (UnknownHostException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    private BasicDBObject getMonogoObect(RFIDTracking rfidTracking) {
        BasicDBObject basicDBObject = new BasicDBObject();
        basicDBObject.append("trackingBraceletId", rfidTracking.getTrackingBraceletId());

        List<BasicDBObject> trackings = new ArrayList<BasicDBObject>();
        trackings.add(newTracking(rfidTracking));
        basicDBObject.put("trackings", trackings);

        return basicDBObject;
    }

    public void insertIntoDb(RFIDTracking rfidTracking) {
        BasicDBObject query = new BasicDBObject("trackingBraceletId", rfidTracking.getTrackingBraceletId());
        BasicDBObject result = (BasicDBObject) dbCollection.findOne(query);
        if (result == null) {
            dbCollection.insert(getMonogoObect(rfidTracking));
        } else {
            insertIntoExistingDoc(rfidTracking, result);
        }
    }

    private void insertIntoExistingDoc(RFIDTracking rfidTracking, BasicDBObject foundDoc) {
        BasicDBObject newTracking = newTracking(rfidTracking);

        BasicDBObject updateCommand = new BasicDBObject();
        updateCommand.put("$push", new BasicDBObject("trackings", newTracking));
        dbCollection.update(foundDoc, updateCommand);
    }

    private BasicDBObject newTracking(RFIDTracking rfidTracking) {
        BasicDBObject basicDBObject = new BasicDBObject();

        basicDBObject.append("time", rfidTracking.getTime());
        basicDBObject.append("type", rfidTracking.getType());
        basicDBObject.append("facilityId", rfidTracking.getFacilityId());

        return basicDBObject;
    }
}

package tracking;

import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;

import java.io.Reader;

public class RFIDMessageSystem {
    public static RFIDTracking getRFIDTracking(Reader reader) {
        RFIDTracking tracking = null;
        try {
            tracking = (RFIDTracking) Unmarshaller.unmarshal(RFIDTracking.class, reader);
        } catch (MarshalException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (ValidationException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        return tracking;
    }


}

package xml;

import model.Festival;
import org.hibernate.Query;
import org.hibernate.Session;
import org.jdom.Document;
import org.xml.sax.SAXException;
import persistency.HibernateUtil;

import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: M
 * Date: 25/10/13
 * Time: 14:23
 * To change this template use File | Settings | File Templates.
 */
public class TestXML {
    public static void main(String[] args) throws IOException, SAXException {
        Session session = HibernateUtil.getSessionFactory().openSession();
        PersBericht bericht = new PersBericht(queryFestival(1, session));

        Document d = null;
        try {
            d = bericht.createSchema();
            Utilities.saveXml(d, "test.xml");
        } catch (MalformedXMLException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }


        session.close();
    }

    private static Festival queryFestival(int id, Session s) {
        Query query = s.createQuery("from Festival f where f.festivalId = :festivalId");
        query.setInteger("festivalId", id);
        return (Festival) query.uniqueResult();
    }


}

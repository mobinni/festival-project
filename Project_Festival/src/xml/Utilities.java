package xml;

import org.jdom.Document;
import org.jdom.JDOMException;
import org.jdom.Namespace;
import org.jdom.input.SAXBuilder;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: M
 * Date: 27/10/13
 * Time: 12:28
 * To change this template use File | Settings | File Templates.
 */
public class Utilities {
    public static Namespace getDefaultNamespace() {
        return Namespace.getNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance");
    }


    public static String getSchemeLocation() {
        return "../XML Schemes/PressRelease2.xsd";
    }


    public static void saveXml(Document document, String xmlTitle) {
        File file = new File(xmlTitle);
        XMLOutputter outputter = new XMLOutputter();
        Format format = Format.getPrettyFormat();
        outputter.setFormat(format);

        try {
            FileWriter writer = new FileWriter(file);
            outputter.output(document, writer);
            writer.flush();
            writer.close();

        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public static Document retrieveDocument(String path) throws JDOMException, IOException {
        File file = new File(path);
        if (file.exists()) {
            Document doc;
            SAXBuilder parser = new SAXBuilder();
            doc = parser.build(file);
            return doc;
        }
        return null;
    }

}

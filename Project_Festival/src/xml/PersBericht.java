package xml;

import helper.DateHelper;
import helper.XMLHelper;
import model.*;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.output.XMLOutputter;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;

/**
 * Created with IntelliJ IDEA.
 * User: M
 * Date: 25/10/13
 * Time: 14:14
 * To change this template use File | Settings | File Templates.
 */
public class PersBericht {
    private Festival festival;

    public PersBericht(Festival festival) {
        this.festival = festival;
    }

    public Document createSchema() throws IOException, SAXException, MalformedXMLException {
        Document doc = new Document();
        Element root = new Element("PressRelease");
        // xsi namespace
        root.addNamespaceDeclaration(Utilities.getDefaultNamespace());

        root.setAttribute("noNamespaceSchemaLocation", Utilities.getSchemeLocation(), Utilities.getDefaultNamespace());

        // Festival element
        Element fest = new Element("Festival");
        fest.setAttribute("endDate", String.valueOf(getLastFestivalDayDate()));
        fest.setAttribute("startDate", String.valueOf(getFirstFestivalDayDate()));

        fest.setAttribute("address", String.valueOf(festival.getLocation().getAddress()));
        fest.setAttribute("name", String.valueOf(festival.getName()));

        for (FestivalDay day : festival.getFestivalDays()) {
            if (day.getPerformances() != null) {
                Element date = new Element("Date");
                date.setAttribute("value", String.valueOf(day.getDate()));

                // add unique stages to date
                for (Facility s : getStagesOfAFestivalDay(day)) {
                    Element stage = new Element("Podium");
                    stage.setAttribute("name", s.getFacility().getName());

                    // add performances to stage
                    for (Artist a : getArtistsOfAFestivalDay(day)) {
                        for (Performance p : day.getPerformances()) {
                            if (p.getFacility().equals(s) && p.getArtist().equals(a)) {
                                Element artist = new Element("Artist");
                                artist.setAttribute("name", a.getName());
                                String permission = a.getArtistInfo().isPermission() ? "allowed" : "not allowed";
                                artist.setAttribute("permission", permission);

                                Element availability = new Element("AvailableAt");
                                availability.setAttribute("facility", String.valueOf(a.getFacility().getFacilityType().getType()));

                                availability.setAttribute("time", DateHelper.addTime(30, p.getEndTime()));

                                Element performance = new Element("Performance");

                                performance.setAttribute("endTime", String.valueOf(DateHelper.FORMAT_TIME2.format(p.getEndTime())));
                                performance.setAttribute("startTime", String.valueOf(DateHelper.FORMAT_TIME2.format(p.getStartTime())));

                                // add performance and availability to artist
                                artist.addContent(performance);
                                artist.addContent(availability);

                                // add artist to stage
                                stage.addContent(artist);
                            }
                        }
                    }


                    // add stages to date
                    date.addContent(stage);
                }
                // add date to festival
                fest.addContent(date);
            }
        }


        // Add to root
        root.addContent(fest);

        doc.addContent(root);

        boolean isValid = XMLHelper.validateXML(Utilities.getSchemeLocation(), new XMLOutputter().outputString(doc));

        if (isValid) return doc;
        else
            throw new MalformedXMLException("The XML document provided is malformed and is not valid compared to the schema used");
    }

    private Date getFirstFestivalDayDate() {
        Collections.sort(festival.getFestivalDays());
        FestivalDay fday = festival.getFestivalDays().get(0);
        return fday.getDate();
    }

    private Date getLastFestivalDayDate() {
        Collections.sort(festival.getFestivalDays());
        FestivalDay fday = festival.getFestivalDays().get(festival.getFestivalDays().size() - 1);
        return fday.getDate();
    }

    private HashSet<Facility> getStagesOfAFestivalDay(FestivalDay day) {
        HashSet<Facility> stages = new HashSet<Facility>();
        if (day.getPerformances() != null) {
            for (Performance p : day.getPerformances()) {
                stages.add(p.getFacility());
            }
        }
        return stages;
    }

    private HashSet<Artist> getArtistsOfAFestivalDay(FestivalDay day) {
        HashSet<Artist> artists = new HashSet<Artist>();
        if (day.getPerformances() != null) {
            for (Performance p : day.getPerformances()) {
                artists.add(p.getArtist());
            }
        }
        return artists;
    }

}

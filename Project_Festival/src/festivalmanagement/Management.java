package festivalmanagement;

import helper.DateHelper;
import model.*;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;
import tracking.RFIDTracking;

import java.text.ParseException;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: Thomas
 * Date: 27/10/13
 * Time: 16:34
 * To change this template use File | Settings | File Templates.
 */
public class Management {
    private Session session;
    private Transaction transaction;
    private Scanner scanner;

    public Management(Session session, Transaction transaction) {
        this.session = session;
        this.transaction = transaction;
        this.scanner = new Scanner(System.in);
    }

    public Festival pickFestival() {
        final Query festivalQuery = session.createQuery("from Festival");
        final List<Festival> festivals = festivalQuery.list();

        System.out.println("Beschikbare festivals: \n");
        for (int i = 0; i < festivals.size(); i++) {
            System.out.printf("%d %s\n", i, festivals.get(i).getName());
        }
        System.out.print("\nUw keuze: ");
        int input = scanner.nextInt();

        if (input < 0 || input >= festivals.size()) return festivals.get(0);

        return festivals.get(input);
    }

    public TicketType pickTicketTypes(Festival festival) {
        System.out.println("Beschikbare tickettypes: \n");
        for (int i = 0; i < festival.getTicketTypes().size(); i++) {
            final TicketType ticketType = festival.getTicketTypes().get(i);

            System.out.printf("%d %s €%.2f\n", i, ticketType.getName(), ticketType.getPrice());
            for (FestivalDay f : ticketType.getFestivalDays()) {
                System.out.printf("\t %s\n", f.getDate().toString());
            }
        }
        System.out.print("\nUw keuze: ");
        int input = scanner.nextInt();

        if (input < 0 || input >= festival.getTicketTypes().size()) return festival.getTicketTypes().get(0);

        return festival.getTicketTypes().get(input);
    }

    public int insertVisitorPass(RFIDTracking rfidTracking, int festivalId) {
        Tracking tracking = new Tracking();
        final Query festivalQuery = session.createQuery("from Festival f where f.festivalId=:festivalId");
        festivalQuery.setInteger("festivalId", festivalId);
        final Festival festival = (Festival) festivalQuery.uniqueResult();
        final Query facilityQuery = session.createQuery("from Facility where id=:facilityId");
        facilityQuery.setInteger("facilityId", rfidTracking.getFacilityId());
        final Facility facility = (Facility) facilityQuery.uniqueResult();
        final Query braceletQuery = session.createQuery("from TrackingBracelet where id=:braceletId");
        braceletQuery.setInteger("braceletId", rfidTracking.getTrackingBraceletId());
        final TrackingBracelet trackingBracelet = (TrackingBracelet) braceletQuery.uniqueResult();


        if (festival != null && facility != null && trackingBracelet != null) {
            tracking.setTime(rfidTracking.getTime());
            tracking.setType((rfidTracking.getType().equalsIgnoreCase("IN")));
            tracking.setFacility(facility);
            tracking.setFestival(festival);
            tracking.setTrackingBracelet(trackingBracelet);

            if (rfidTracking.getType().equalsIgnoreCase("IN")) {
                final Query performanceQuery = session.createQuery("from Performance per WHERE per.facility.facility.facilityId = :guestZoneId AND :time > per.startTime AND :time < per.endTime");
                performanceQuery.setInteger("guestZoneId", rfidTracking.getFacilityId());
                performanceQuery.setParameter("time", rfidTracking.getTime());
                final List<Performance> performances = performanceQuery.list();

                if (performances != null && performances.size() > 0) {
                    for (Performance performance : performances) {
                        final SQLQuery performanceTrackingQuery = session.createSQLQuery("INSERT INTO t_performance_tracking (performanceId,numberOfTrackings) VALUES (:performanceId,1) ON DUPLICATE KEY UPDATE numberOfTrackings=numberOfTrackings+1");
                        performanceTrackingQuery.setInteger("performanceId", performance.getPerformanceId());
                        performanceTrackingQuery.executeUpdate();
                    }
                }
            } else {
                final Query searchInEquivalent = session.createQuery("from Tracking trac WHERE trac.facility.facilityId = :facId AND trac.type=:trackingType AND trac.trackingBracelet.braceletId = :braceletId AND trac.time < :rfidtime");
                searchInEquivalent.setInteger("facId", rfidTracking.getFacilityId());
                searchInEquivalent.setBoolean("trackingType", true);
                searchInEquivalent.setInteger("braceletId", rfidTracking.getTrackingBraceletId());
                searchInEquivalent.setParameter("rfidtime", rfidTracking.getTime());
                Tracking inTracking = (Tracking) searchInEquivalent.uniqueResult();

                if (inTracking != null) {
                    long timeDiff = (rfidTracking.getTime().getTime() - inTracking.getTime().getTime()) / 1000;
                    inTracking.getFacility().getVisitTimes().add(timeDiff);
                }
            }
        } else return -1;
        session.saveOrUpdate(tracking);
        return 0;
    }

    public int buyTickets(Festival festival, TicketType ticketType, SalesPoint salesPoint) {
        System.out.println("Hoeveel tickets wilt u bestellen? ");
        System.out.print("Uw keuze: ");
        int input = scanner.nextInt();
        if (input <= 0) input = 1;

        for (int i = 0; i < input; i++) {
            Ticket ticket = new Ticket();
            int age = new Random().nextInt(85) + 16;
            if (age >= 16 && age < 18) {
                ticket.setAgeGroup(festival.getAgeGroups().get(0));
            } else if (age >= 18 && age < 21) {
                ticket.setAgeGroup(festival.getAgeGroups().get(1));
            } else if (age >= 21 && age < 25) {
                ticket.setAgeGroup(festival.getAgeGroups().get(2));
            } else {
                ticket.setAgeGroup(festival.getAgeGroups().get(3));
            }
            ticket.setAge(age);
            TrackingBracelet bracelet = new TrackingBracelet();
            ticket.setSalesPoint(salesPoint);
            ticket.setTicketType(ticketType);
            ticket.setTrackingBracelet(bracelet);
        }

        session.saveOrUpdate(festival);
        transaction.commit();
        if (transaction.wasCommitted()) {
            return input;
        }
        return 0;
    }

    public Date askTime() throws ParseException {
        System.out.print("Uur: ");

        final int hour = scanner.nextInt();
        System.out.print("Minuten: ");
        scanner.nextLine();

        final int minutes = scanner.nextInt();
        if (!DateHelper.isValidTime(hour, minutes)) throw new IllegalArgumentException("Not a valid time");
        scanner.nextLine();
        return DateHelper.createTime(hour, minutes);
    }

    public List<Performance> getPerformancesAt(Date time, Facility facility) {
        final Query query = session.createQuery("from Performance p where p.facility.id = :id and p.startTime <= :time and p.endTime >= :time");
        query.setInteger("id", facility.getFacilityId());
        query.setParameter("time", time);

        return query.list();
    }

    public List<Festival> getFestivalsOfArtistBetween(Artist artist, Date startTime, Date endTime){
        final Query query = session.createQuery("select perf.festival from Performance perf where perf.artist.artistId = :artistId AND perf.startTime >= :startTime AND perf.endTime <= :endTime");
        query.setInteger("artistId", artist.getArtistId());
        query.setParameter("startTime", startTime);
        query.setParameter("endTime", endTime);

        return query.list();
    }
}

package festivalmanagement;

import helper.DateHelper;
import helper.NameGenerator;
import model.*;
import org.hibernate.Session;
import org.hibernate.Transaction;
import persistency.HibernateUtil;

import java.text.ParseException;
import java.util.Date;
import java.util.Random;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * User: M
 * Date: 28/10/13
 * Time: 9:55
 * To change this template use File | Settings | File Templates.
 */
public class startAddPerformance {
    private final static NameGenerator NAME_GENERATOR = new NameGenerator();
    private final static Random RANDOM = new Random();

    public static void main(String[] args) throws ParseException {
        final Session session = HibernateUtil.getSessionFactory().openSession();
        final Transaction transaction = session.beginTransaction();
        final Management management = new Management(session, transaction);
        final Festival festival = management.pickFestival();

        Scanner scanner = new Scanner(System.in);
        System.out.println("Pick an artist from the list");
        for (int i = 0; i < festival.getArtistInfos().size(); i++) {
            System.out.println(i + " " + festival.getArtistInfos().get(i).getArtist().getName());
        }
        int artistId = scanner.nextInt();
        // Artist to add performance to
        Artist a = festival.getArtistInfos().get(artistId).getArtist();

        System.out.println("Performance specifics");
        Performance p = new Performance();
        p.setArtist(a);
        p.setFestival(festival);

        System.out.println("Start Time:");
        Date startTime = null;
        try {
            startTime = management.askTime();
        } catch (ParseException e) {
            System.out.println("Could not parse");
        }

        System.out.println("End Time:");
        Date endTime = null;
        try {
            endTime = management.askTime();
        } catch (ParseException e) {
            System.out.println("Could not parse");
        }

        // Check start and end time
        if (startTime.getTime() < endTime.getTime()) {
            p.setStartTime(startTime);
            p.setEndTime(endTime);
        } else {
            throw new IllegalArgumentException("The start time may not be after than the end time of a performance or vice versa");
        }
        // Facility to add
        Facility f = null;

        System.out.println("Pick a stage");
        for (int i = 0; i < festival.getFacilities().size(); i++) {
            f = festival.getFacilities().get(i);
            if (f.getFacilityType().getType().equals("Stage")) {
                System.out.println(i + " " + f.getName());
            }
        }
        System.out.println("Your choice: ");
        int facilityId = scanner.nextInt();
        p.setFacility(festival.getFacilities().get(facilityId));

        SoundCheck soundCheck = new SoundCheck(DateHelper.createTime(10, 0), DateHelper.createTime(10, 5));
        p.setSoundCheck(soundCheck);

        for (int i = 0; i < 5; i++) {
            Need n = new Need();
            n.setAmount(i * 2);

            Need need = new Need(NAME_GENERATOR.getName(), RANDOM.nextInt(8) + 1);
            p.addNeed(need);
        }

        session.save(p);
        transaction.commit();
        session.close();

    }
}

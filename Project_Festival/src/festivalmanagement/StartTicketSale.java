package festivalmanagement;

import model.Festival;
import model.SalesPoint;
import model.TicketType;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import persistency.HibernateUtil;

/**
 * Created with IntelliJ IDEA.
 * User: Thomas
 * Date: 27/10/13
 * Time: 14:40
 * To change this template use File | Settings | File Templates.
 */
public class StartTicketSale {


    public static void main(String[] args) {
        final Session session = HibernateUtil.getSessionFactory().openSession();
        final Transaction transaction = session.beginTransaction();
        final Management management = new Management(session, transaction);
        final Query websaleQuery = session.createQuery("from SalesPoint s where s.name = :naam");
        websaleQuery.setString("naam", "WebsiteSalesPoint");

        final Festival festival = management.pickFestival();
        final TicketType ticketType = management.pickTicketTypes(festival);

        System.out.printf("U heeft %d tickets gekocht", management.buyTickets(festival, ticketType, (SalesPoint) websaleQuery.list().get(0)));

    }
}

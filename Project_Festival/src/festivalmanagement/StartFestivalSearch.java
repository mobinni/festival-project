package festivalmanagement;

import model.Artist;
import model.Festival;
import model.Performance;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import persistency.HibernateUtil;

import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Random;

/**
 * Created with IntelliJ IDEA.
 * User: Thomas
 * Date: 1/11/13
 * Time: 23:56
 * To change this template use File | Settings | File Templates.
 */
public class StartFestivalSearch {
    public static void main(String[] args) {
        final Session session = HibernateUtil.getSessionFactory().openSession();
        final Transaction transaction = session.beginTransaction();
        final Management management = new Management(session, transaction);
        final Random random = new Random();

        try {
            final Date startTime = management.askTime();
            final Date endTime = management.askTime();

            final Query artistQuery = session.createQuery("from Artist");
            final List<Artist> artists = artistQuery.list();
            final Artist randomArtist = artists.get(random.nextInt(artists.size()));

            System.out.printf("Random Artist: %s heeft gespeeld op de volgende festival(s): \n", randomArtist.getName());
            for(Festival festival : management.getFestivalsOfArtistBetween(randomArtist, startTime, endTime)){
                System.out.println(festival.getName());
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
}

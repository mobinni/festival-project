package festivalmanagement;

import model.Facility;
import model.Festival;
import model.Performance;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import persistency.HibernateUtil;

import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Random;

/**
 * Created with IntelliJ IDEA.
 * User: Thomas
 * Date: 27/10/13
 * Time: 16:59
 * To change this template use File | Settings | File Templates.
 */
public class StartPerformanceSearch {
    public static void main(String[] args) {
        final Session session = HibernateUtil.getSessionFactory().openSession();
        final Transaction transaction = session.beginTransaction();
        final Management management = new Management(session, transaction);
        final Random random = new Random();


        try {
            final Festival festival = management.pickFestival();
            final Date time = management.askTime();

            System.out.println("\nEen random facility: ");
            Facility facility = festival.getFacilities().get(random.nextInt(festival.getFacilities().size()));
            for (Performance performance : management.getPerformancesAt(time, facility)) {
                System.out.println(performance.getPerformanceId());
            }

            System.out.println("\nEen GuestZone");
            final Query facilityQuery = session.createQuery("from Facility f where f.facilityType.type = :type and f.festival.id = :festivalId");
            facilityQuery.setString("type", "Stage");
            facilityQuery.setInteger("festivalId", festival.getFestivalId());
            final List<Facility> fs = facilityQuery.list();

            final List<Performance> performances = management.getPerformancesAt(time, fs.get(random.nextInt(fs.size())));

            for (Performance performance : performances) {
                System.out.printf("%d - %s\n", performance.getPerformanceId(), performance.getName());
            }
        } catch (ParseException e) {
            System.out.println("Could not parse");
        }


    }
}

import org.apache.activemq.ActiveMQConnectionFactory;
import org.exolab.castor.xml.Marshaller;

import javax.jms.*;
import javax.xml.bind.JAXBException;
import java.io.StringWriter;
import java.util.Date;
import java.util.Random;

public class RFIDSystem {
    private static final int NUMBER_OF_PEOPLE = 15000;
    private static final int NUMBER_OF_ZONES = 20;

    public static void main(String[] args) throws JAXBException {
        ActiveMQConnectionFactory connectionFactory =
                new ActiveMQConnectionFactory("failover:(tcp://localhost:60601,tcp://localhost:60602)?timeout=1000");

        try {
            //Create a connetion to ActiveMQ
            Connection connection = connectionFactory.createConnection();
            connection.start();

            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

            Queue destination = session.createQueue("TEST.SENDRECEIVE");

            MessageProducer messageProducer = session.createProducer(destination);
            messageProducer.setDeliveryMode(DeliveryMode.PERSISTENT);

            StringWriter writer = new StringWriter();

            //Random tracking creations (50000 ppl * 20 zone changes( = 2 tracking messages))
            //40 zones
            for (Integer i = 0; i < 1000; i++) {
                Random randomGenerator = new Random();
                Integer randomBraceletId = randomGenerator.nextInt(NUMBER_OF_PEOPLE);
                Integer randomZoneIdIn, randomZoneIdOut;

                //in and out zoneID cant be the same
                do {
                    randomZoneIdIn = randomGenerator.nextInt(NUMBER_OF_ZONES) + 1;
                    randomZoneIdOut = randomGenerator.nextInt(NUMBER_OF_ZONES) + 1;
                } while (randomZoneIdIn == randomZoneIdOut);

                Date timestamp = DateHelper.createTime(randomGenerator.nextInt(8) + 10, randomGenerator.nextInt(60));
                RFIDTracking out = new RFIDTracking(i * 2, timestamp, "OUT", randomBraceletId, randomZoneIdOut);
                RFIDTracking in = new RFIDTracking(i * 2 + 1, timestamp, "IN", randomBraceletId, randomZoneIdIn);

                TextMessage message;

                //send outtracking
                Marshaller.marshal(out, writer);
                message = session.createTextMessage(writer.toString());
                messageProducer.send(message);
                writer.getBuffer().setLength(0);

                //send intracking
                Marshaller.marshal(in, writer);
                message = session.createTextMessage(writer.toString());
                messageProducer.send(message);
                writer.getBuffer().setLength(0);
            }

            session.close();
            connection.close();
            messageProducer.close();
            System.exit(0);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

import org.apache.activemq.ActiveMQConnectionFactory;
import org.exolab.castor.xml.Marshaller;

import javax.jms.*;
import javax.jms.Queue;
import javax.xml.bind.JAXBException;
import java.io.StringWriter;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: Thomas
 * Date: 1/11/13
 * Time: 21:54
 * To change this template use File | Settings | File Templates.
 */
public class RFIDSystemLessRandom {
    private static final int NUMBER_OF_PEOPLE = 15000;
    private static final int NUMBER_OF_ZONES = 20;

    public static void main(String[] args) throws JAXBException {
        ActiveMQConnectionFactory connectionFactory =
                new ActiveMQConnectionFactory("failover:(tcp://localhost:60601,tcp://localhost:60602)?timeout=1000");

        try {
            //Create a connetion to ActiveMQ
            Connection connection = connectionFactory.createConnection();
            connection.start();

            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

            Queue destination = session.createQueue("TEST.SENDRECEIVE");

            MessageProducer messageProducer = session.createProducer(destination);
            messageProducer.setDeliveryMode(DeliveryMode.PERSISTENT);

            StringWriter writer = new StringWriter();

            for (Integer i = 0; i < 1000; i++) {
                Random randomGenerator = new Random();
                Integer randomBraceletId = randomGenerator.nextInt(NUMBER_OF_PEOPLE);
                Integer randomZoneId = randomGenerator.nextInt(NUMBER_OF_ZONES) + 1;

                Date inDate = DateHelper.createTime(randomGenerator.nextInt(8) + 10, randomGenerator.nextInt(60));
                Date outDate = DateHelper.addTime(randomGenerator.nextInt(120) + 1, inDate);

                RFIDTracking in = new RFIDTracking(i * 2 + 1, inDate, "IN", randomBraceletId, randomZoneId);
                RFIDTracking out = new RFIDTracking(i * 2, outDate, "OUT", randomBraceletId, randomZoneId);
                TextMessage message;

                //send intracking
                Marshaller.marshal(in, writer);
                message = session.createTextMessage(writer.toString());
                messageProducer.send(message);
                writer.getBuffer().setLength(0);

                //send outtracking
                Marshaller.marshal(out, writer);
                message = session.createTextMessage(writer.toString());
                messageProducer.send(message);
                writer.getBuffer().setLength(0);
            }

            session.close();
            connection.close();
            messageProducer.close();
            System.exit(0);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
